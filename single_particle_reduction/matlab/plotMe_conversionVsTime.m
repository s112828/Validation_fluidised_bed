%Function to calculate a second stage reaction
%Formulars according Wen, "Noncatalytic Heterogenous Solid Fluid Reaction Models"
%models p. 42-44
clear
clc
close all
more off

%% User input 
run('parametersHetReaction.m')
relativePath  = 'E:\TUe\Afstuderen\Simulations\Reduction\Single_particle\final_ironoxide_10sec_T1500K\';

run('parametersHetReaction.m')

fileToLoad1  = 'speciesSolidAv.json';
fileToLoad2  = 'heat.json';
fileToLoad3  = 'heatAv.json';
fileToLoad4  = 'speciesSolid.json';

verificationFileName = '../verificationStats/errorConversion.json';

particleIndex = 1;
nPlotPoints   = 100;
plotSkip      = 1;


%% MAIN PROGRAM 
%1 - Get data from ParScale, this will be unordered
myFiles1 = dir([relativePath,'*0.5*']);
myFiles2 = dir([relativePath,'*1.*']);
myFiles3 = dir([relativePath,'*2.*']);
myFiles4 = dir([relativePath,'*3.*']);
myFiles5 = dir([relativePath,'*4.*']);
myFiles6 = dir([relativePath,'*5.*']);
myFiles7 = dir([relativePath,'*6.*']);
myFiles8 = dir([relativePath,'*7.*']);
myFiles9 = dir([relativePath,'*8.*']);
myFiles10 = dir([relativePath,'*9.*']);
myFiles11 = dir([relativePath,'*10.*']);
myFiles  = [myFiles1; myFiles2; myFiles3; myFiles4; myFiles5; myFiles6; myFiles7; myFiles8; myFiles9; myFiles10; myFiles11];

validData = 0;
Temp      = zeros(1,10);

for iDir=1:size(myFiles,1)
    if(myFiles(iDir).isdir)
        validData = validData +1;
        [xDat1, yDat1, misc1] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'/',fileToLoad1], ...
            'data',particleIndex);
        
        [xDat2, yDat2, misc2] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'/',fileToLoad2], ...
            'data',particleIndex);
        
        yDat2 = cell2mat(yDat2);
        Temp(validData,:) = yDat2(1,1:10);
        
        parScaleConversion(validData) = 1 - (yDat1./Cs0);
        parScaleTime(validData)       = str2num(myFiles(iDir).name);
    end
end

%% Calculation Thielle Moduls
%%%%% USER INPUT END %%%%%%%
Rc            = 8314.4621;    % Universal gasconstant
EA            = 76;           % Activation energy
A0            = 2.2e3;        % Frequency factor
T             = 1500;         % Initial temperature

k_v           = (A0*exp(-EA./(Rc.*T)))        % Reaction rate constant
ThieleMod     = R * sqrt((a.*k_v)./(DeIA))    % Thiele modulus
N_sh          = k_mA * R/D_eA;                % mass transfer/diffusion
D_ratio       = DeIA/D_eA;                    % Ratio diffusion

disp('Diffusion time scale');
diffusionTimeScale = R.^2./DeIA

disp('Reaction time scale');
reactionTimeScale = 1./(k_vIntr*CA0)

%%  Calculate dimensionless variables

parScaleEndTime         = max(parScaleTime);
parScaleEndTimePosition = parScaleTime==parScaleEndTime;
parScaleFinalConversion = parScaleConversion(parScaleEndTimePosition);
parScaleEndTimeDimLess  = parScaleEndTime./reactionTimeScale ;
parScaleDimLessTime     = parScaleTime./reactionTimeScale ;

%% save the key results

results.Cs0             = Cs0;
results.conversion      = parScaleFinalConversion;
results.time            = parScaleEndTime;
disp('final conversion:');
results.conversion
disp('final solids consumption:');
results.consumption = results.conversion*Cs0
disp('after:');
results.time
jSonDat = savejson('',results, 'result.json');

%% Plot the analytical solution

omegaPlot    = linspace(0, parScaleEndTimeDimLess, nPlotPoints);

for iP=1:nPlotPoints
    xi_m(iP)       = Wen_xi_m_time(omegaPlot(iP),D_ratio,ThieleMod,N_sh);
    conversion(iP) = Wen_conversion(ThieleMod, xi_m(iP), N_sh, D_ratio, omegaPlot(iP));
end

plot(omegaPlot,conversion,'b-','MarkerFaceColor','b','Markersize',5,'LineWidth',1)
hold on

%% compute the error

for iP=1:validData
    xi_m_verification(iP)       = Wen_xi_m_time(parScaleDimLessTime(iP),D_ratio,ThieleMod,N_sh);
    conversion_verification(iP) = Wen_conversion (ThieleMod, xi_m_verification(iP), N_sh, D_ratio, parScaleDimLessTime(iP));
end

verification.nSamples      = length(conversion_verification);
verification.ErrorPosition = parScaleDimLessTime';
verification.Error         = (conversion_verification - parScaleConversion)';
verification.ErrorMax      = max(abs(verification.Error));
verification.ErrorMean     = sqrt(mean(verification.Error.^2));
disp('Mean error in conversion:');
verification.ErrorMean
disp('Max error in conversion:');
verification.ErrorMax

%% Force code conversion stages 1 and 2

x1 = 1;       % Stage 1 will always end at 1 
x2 = 1.7761;  % Set this value to the last printed omega_v when running script!!

%% ParScale Plot START
plot(parScaleDimLessTime(1:plotSkip:end),parScaleConversion(1:plotSkip:end),'bo','MarkerFaceColor','r','Markersize',5,'LineWidth',1)
ylim([0,1]);
plot([x1 x1], ylim,'r--','LineWidth',2) 
plot([x2 x2], ylim,'r--','LineWidth',2) 
hold on

%% ParScale Plot END \grid on;
set(gca,'Fontsize',14)
hLeg=legend('Analytical','ParScale');
set(hLeg,'location','Northwest','FontSize',14);
xlabel('t / t_{react} (-)','FontSize',14)
ylabel('X_s (-)','FontSize',14)
xlim([0,parScaleEndTimeDimLess]);

dim = [.4 .1 .3 .3];
str = '1st stage';
annotation('textbox',dim,'String',str,'FitBoxToText','on');

dim = [.7 .1 .3 .3];
str = '2nd stage';
annotation('textbox',dim,'String',str,'FitBoxToText','on');

dim = [.75 .1 .3 .3];
str = '3rd stage';
annotation('textbox',dim,'String',str,'FitBoxToText','on');

print('-dpng ','conversion')
