R    = 5e-4;            % (outer) radius of the particle, m
k_mA = 10;              % External mass transfer coefficient, m/s
                        % must be set to large if Dirichlet BC is applied!
porosity = 0.2;         % Porosity
D_Mol    = 2e-4;        % Molecular rate of diffusion, typical for oxygen in air
D_eA = D_Mol*porosity;  % Effective rate of diffusion
DeIA = D_eA;            % Rate of diffusion of A (fluid) in reaction zone
CA0 = 2.12e-3;          % Initial concentration of gas phase, in kmol/m³_gas, for y=0.20 at 1 bar and 1098 K
Cs0 = 26.26;            % Initial concentration of solid, in kmol/m³_tot, for Cu and a volume fraction of 0.1 in the particle
a   = 0.33333;          % Stoichometric coeff: ratio of gas to solid stoichimetric ratio
radius_interval = 1e-5; % Interval for radius output 


