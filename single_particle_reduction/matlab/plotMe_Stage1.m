%Function to calculate a first-stage reaction
%Formulars according Wen, "Noncatalytic Heterogenous Solid Fluid Reaction Models"
%models p. 42-44

clear
clc
close all
more off

%add the path to functions for postprocessing
%PARSCALE_SRC_DIR = getenv('PASCAL_SRC_DIR');
%if(isempty(PARSCALE_SRC_DIR))
%    error('The user has not set the environment variable "PASCAL_SRC_DIR". Please do so, e.g., in your .bashrc file, to use this octave script.')
%end
%addpath([PARSCALE_SRC_DIR,'/../examples/octaveFunctions']);


%%%% GRAPHICS/OUTPUT SETTINGS %%%%%
%run('formatting.m')

%%%%% USER INPUT START %%%%%%%%%%%%%
run('parametersHetReaction.m')
%timeToPlot  = '0.200000';
%timeToPlot  = '0.400000'; 
%timeToPlot  = '1.000100'
timeToPlot  = '2.000100'
%timeToPlot  = '6.000000';  

         
%%%%% USER INPUT END %%%%%%%%%%%%%%%
time_ParScale = str2num(timeToPlot);    %simulation time  

Rc            = 8.3144621;
EA            = 76e3;
A0            = 2.2e3;
T             = 775;

k_v           = (A0*exp(-EA./(Rc*T)))*Cs0
k_vIntr       = k_v/Cs0

ThieleMod = R * sqrt( a*k_vIntr*Cs0/ D_eA)
Omega_v   = k_vIntr * CA0 * time_ParScale
N_sh      = (k_mA * R)/(D_eA)                %reat of external mass transfer/diffusion 

Omega_vc = Wen_omegaStage1(ThieleMod, N_sh);

disp('Time for complete first stage');
t_complete_first_stage = Omega_vc/(k_vIntr*CA0)


%%%%%%%%%%%%%%ANALYTICAL SOLUTION 
    r = 1e-16;          % small number
    i = 1;

    while (r<=R)
        absolut_radius(i) = r;
        zeta   = r/R;      %dimensionless radius running variable
        dCa(i) = (1/Omega_vc) * ((sinh(zeta*ThieleMod))/(zeta*sinh(ThieleMod)));
        Cs(i)  = (1- ((sinh(zeta*ThieleMod))/(zeta*sinh(ThieleMod))) * (Omega_v / Omega_vc));

        i=i+1;    
        r = r+radius_interval;
    end     

    plot(absolut_radius/R,dCa, ...
           '-','Markersize',5,'LineWidth',2);
    hold on;
    plot(absolut_radius/R,Cs, ...
           'r--','Markersize',5,'LineWidth',2);
    hold on;
    
% Plot for ParScale
%%%%%%%%%%%%%%%%%% FLUID PLOT %%%%%%%%%%%%%%%%%%%%%%%%
i=1;
cd 'E:\TUe\Afstuderen\Simulations\Reduction\Single_particle\test_case\2.000100'
fileName                = 'speciesA.json'; 
[valuesX,particle_data] = jsonGetParScaleData(fileName,'data',i) ;
plot(valuesX,particle_data./CA0,'bo', ...
      'MarkerFaceColor','b','Markersize',5,'LineWidth',2)
hold on

%Initial distribution
i=1;
cd 'E:\TUe\Afstuderen\Simulations\Reduction\Single_particle\test_case\0'
fileName                = 'speciesA.json';
[valuesX,particle_data] = jsonGetParScaleData(fileName,'data',i) ;
plot(valuesX,particle_data./CA0,'bo','Markersize',5,'LineWidth',2)

%%%%%%%%%%%%%%%%%% SOLID PLOT %%%%%%%%%%%%%%%%%%%%%%%%
cd 'E:\TUe\Afstuderen\Simulations\Reduction\Single_particle\original_single_particle\2.000100'
i=1;
fileName                = 'speciesSolid.json';
[valuesX,particle_data] = jsonGetParScaleData(fileName,'data',i) ;
plot(valuesX,particle_data./Cs0,'ro','Markersize',5,'LineWidth',2)
hold on
        
set(gca,'Fontsize',14)
hLeg= legend('Analytical Fluid','Analytical Solid','ParScale Fluid','ParScale Fluid (init.)','ParScale Solid');
set(hLeg,'box','off')
set(hLeg,'location','NorthOutside','FontSize',14)
xlabel('r/R','FontSize',14)
ylabel('C_i / C_{i,0} , i=A,S','FontSize',14)

xlim([0,1.05]);
ylim([0,1]);
title(['t = ', num2str(time_ParScale,'%.3f'),' [s]']);

print('-dpng ','concProfile_Stage1')

