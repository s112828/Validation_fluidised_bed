%% Analysis Thielle modulus

clc; close all; clear all;

%% Input values

timestep = 50;

W     = 0.044;    % Width
dW    = W/18;     % Delta width
L     = 0.12;     % Length
dL    = L/36;     % Delta length
B     = 0.01;     % Depth
dB    = B/1;      % Delta depth
Lsi   = 0.03;
Ls    = 0.03;     % Static bed height
Vb    = W*B*Lsi;  % Volume bed
Vcell = dW*dB*dL; % Volume grid cell
A     = W*B;      % Massflow surface

x     = 0:dW:W;
y     = 0:dB:B;
z_tot = 0:dL:L;
z     = 0:dL:Ls;


%% Parameters gas (air)

U     = 4.5;        % Superficial gas velocity
rho_g = 0.0899;     % Density gas
nu    = 2e-04;      % Kinematic viscosity
g     = 9.81;       % Gravitational acceleration
p     = 101010.101; % Outlet pressure
Cp_g  = 14266;      % Heat capacity gas
k_g   = 0.3838;     % Thermal conductivity gas

%% Parameters solid 

Np      = 9200;              % Number of particles
dp      = 0.0012;            % Particle diameter
rp      = dp/2;              % Particle radius
phi     = 1;                 % Sphericity
rho_s   = 5200;              % Density solid
Vp      = (4/3)*pi*rp^3;     % Volume particle
Vs      = Np*Vp;             % Total volume solid
eps     = (Vb-Vs)/Vb;        % Volume fraction
eps_mf  = 0.4;
Cp_s    = 104;               % Heat capacity
k_s     = 0.58;              % Thermal conductivty
Tp_i    = 550;               % Initial particle temperature
Ap      = 4*pi*rp^2;         % Surface particle
Aps     = (6*(1-eps))/dp;    % Specific fluid-particle heat transfer surface
As      = Np*Aps;            % Specific fluid-particle heat transfer surface
gamma_b = 0.001;             % Volume of solids dispered in bubbles
eta_h   = 1;                 % adsorption efficiency, see book for argument
Conc_i  = 26.26;

%% Calculation Thielle

Rc       = 8314.4621;       % Gasconstante
EA       = 76;              % Activation energy
A0       = 2.2e3;           % Frequency factor
T        = 0:10:1500;       % Temperature range

R        = 5e-4;            % (outer) radius of the particle, m
k_mA     = 10;              % Dxternal mass transfer coefficient, m/s must be set to large if Dirichlet BC is applied!
porosity = 0.2;             % Porosity
D_Mol    = 2e-4;            % Molecular rate of diffusion, typical for oxygen in air
D_eA     = D_Mol*porosity;  % Effective rate of diffusion
DeIA     = D_eA;            % Rate of diffusion of A (fluid) in reaction zone
CA0      = 2.11e-3;         % Initial concentration of gas phase, in kmol/m³_gas
Cs0      = 26.26;           % Initial concentration of solid, in kmol/m³_tot
a        = 1/3;             % Stoichometric coeff: ratio of gas to solid stoichimetric ratio
radius_interval = 1e-5;     % Interval for radius output 

k_v           = (A0*exp(-EA./(Rc.*T)));    % Reaction rate constant 
ThieleMod     = R * sqrt((a*k_v)/(DeIA));  % Thiele modulus
N_sh          = k_mA * R/D_eA;             % Mass transfer/diffusion
D_ratio       = DeIA/D_eA;                 % Ratio diffusions

disp('Diffusion time scale');
diffusionTimeScale = R.^2./DeIA;

disp('Reaction time scale');
reactionTimeScale = 1./(k_vIntr*CA0);

%% Plot Thiele modulus

figure(1)
box on
grid on
hold on
plot(T,ThieleMod,'LineWidth',2)
xlabel('Temperature (K)','FontSize',14)
ylabel('Thiele Modulus (-)','FontSize',14)
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\thiellevsT.png';
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\thiellevsT.fig';
% saveas(gcf,filesave)

%%
figure(2)
box on
grid on
hold on
plot(T,k_v,'LineWidth',2)
xlabel('Temperature (K)','FontSize',14)
ylabel('Reaction rate constant (1/s)','FontSize',14)
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reactionrate.png';
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reactionrate.fig';
% saveas(gcf,filesave)










