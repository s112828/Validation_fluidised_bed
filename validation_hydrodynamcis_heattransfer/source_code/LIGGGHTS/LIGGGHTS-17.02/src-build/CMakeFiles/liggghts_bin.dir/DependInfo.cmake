# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/s112882/LIGGGHTS/LIGGGHTS-17.02/src/main.cpp" "/home/s112882/LIGGGHTS/LIGGGHTS-17.02/src-build/CMakeFiles/liggghts_bin.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/lib/openmpi/include/openmpi/opal/mca/event/libevent2021/libevent"
  "/usr/lib/openmpi/include/openmpi/opal/mca/event/libevent2021/libevent/include"
  "/usr/lib/openmpi/include"
  "/usr/lib/openmpi/include/openmpi"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/s112882/LIGGGHTS/LIGGGHTS-17.02/src-build/CMakeFiles/liggghts.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
