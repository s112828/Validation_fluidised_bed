%% This script is used to calculate the heat transfer in a single bubble per simulation

clear all; clc;

cd 'E:\TUe\Afstuderen\Simulations\Heat_transfer'  % Path to the right input files

%% Dimensions initial bed

W     = 0.21;     % Width
dW    = W/30;     % Delta width
L     = 0.63;     % Length
dL    = L/84;     % Delta length
B     = 0.018;    % Depth
dB    = B/1;      % Delta depth
Lsi   = 0.28;
Ls    = 0.3;      % Static bed height
Vb    = W*B*Lsi;  % Volume bed
Vcell = dW*dB*dL; % Volume grid cell
A     = W*B;      % Massflow surface

x1  = 0:dW:14*dW;       % x positions box 1
x2  = 15*dW:dW:16*dW;   % x positions box 2
x3  = 17*dW:dW:30*dW;   % x positions box 3

y     = 0:dB:B;    % y positions
z_tot = 0:dL:L;    % z positions total
z     = 0:dL:Ls;   % z positions bed

Ncell_x = length(x1)+length(x2)+length(x3)-1;

Nprobx1 = length(x1)*length(y)*length(z_tot);
Nprobx2 = length(x2)*length(y)*length(z_tot);
Nprobx3 = length(x3)*length(y)*length(z_tot);

Nprobes = Nprobx1+Nprobx2+Nprobx3;

%% Parameters gas (air)

U     = 30;                  % Superficial gas velocity
rho_g = 0.702;               % Density gas
nu    = 3.782e-05;           % Kinematic viscosity
mu    = 2.607e-05;           % Dynamic viscosity
g     = 9.81;                % Gravitational acceleration
p     = 101010.101;          % Outlet pressure
Cp_g  = 1029.5;              % Heat capacity gas
k_g   = 0.04041;             % Thermal conductivity gas

%% Parameters solid (glass)

Np      = 126337;            % Number of particles
dp      = 0.002;             % Particle diameter
rp      = dp/2;              % Particle radius
phi     = 1;                 % Sphericity
rho_s   = 2526;              % Density solid
Vp      = (4/3)*pi*rp^3;     % Volume particle
Vs      = Np*Vp;             % Total volume solid
eps     = (Vb-Vs)/Vb;        % Volume fraction
eps_mf  = 0.4;               % Minimal fluidisation volume fraction
Cp_s    = 840;               % Heat capacity
k_s     = 1.05;              % Thermal conductivty
Te      = 300;               % Initial particle temperature
Ap      = 4*pi*rp^2;         % Surface particle
Aps     = (6*(1-eps))/dp;    % Specific fluid-particle heat transfer surface
As      = Np*Aps;            % Specific fluid-particle heat transfer surface
gamma_b = 0.001;             % Volume of solids dispered in bubbles
eta_h   = 1;                 % adsorption efficiency, see book for argument

%% Calculation minimum fluidisation velocity

Umf1       = (dp^2*(rho_s-rho_g)*g)/(150*mu)*(eps_mf^3*phi^2)/(1-eps_mf);
Umf2       = sqrt((dp*(rho_s-rho_g)*g)/(1.75*rho_g)*eps_mf^3*phi);

Re_p_check = (dp*Umf1*rho_g)/mu;

if Re_p_check < 20

    Umf = (dp^2*(rho_s-rho_g)*g)/(150*mu)*(eps_mf^3*phi^2)/(1-eps_mf);

else

    Umf = sqrt((dp*(rho_s-rho_g)*g)/(1.75*rho_g)*eps_mf^3*phi);

end

fprintf('Reynolds check = %f [-]\n',Re_p_check)
fprintf('Minimum fluidizing velocity = %f m/s\n',Umf)

%% Load data particles
%%% Make sure the right path is set to the input files genereated in the
%%% DEM part.

path         = 'E:\TUe\Afstuderen\Simulations\Heat_transfer\t_06sec_Ti500k_117m\dump160000.liggghts_run';
columns1     = 20;
headerlines1 = 9;
dataSim      = loaddata(path,columns1,headerlines1);
dataSim      = transpose(dataSim);
dataSim      = sortrows(dataSim,1);
t            = 10000;
files        = 160000+t:t:750000;
starttime    = 0;
endtime      = 1;
time         = starttime:(endtime-starttime)/length(files):endtime;
q            = 1;


for k = 160000+t:t:750000;
    addpath('E:\TUe\Afstuderen\Simulations\Heat_transfer\t_06sec_Ti500k_117m')

    matFileName = sprintf('dump%d.liggghts_run',k);
    if exist(matFileName, 'file')
        cd E:\TUe\Afstuderen\Matlab\Scripts;
        dataSim1 = loaddata(matFileName,columns1,headerlines1);

    else
        fprintf('File %s does not exist.\n', matFileName);
    end

    dataSim1 = transpose(dataSim1);
    dataSim1 = sortrows(dataSim1,1);
    dataSim  = [dataSim dataSim1];

end

%%% Save particle heights and find highest particle

q = 1;
for j = 0:columns1:length(files)*columns1   % Use for 2 mm particles
% for j = 0:columns1:9*columns1             % Use for 1 mm particles
    Z(:,q)  = dataSim(:,5+j);
    q=q+1;
end


Heightp       = 0.55.*max(Z);
Heightp(1,61) = 0;
zp            = ceil(Heightp./dL);


% cd E:\TUe\Afstuderen\Simulations\Heat_transfer
% save('data_Z_900K_2mm','Heightp','zp')

%% Load height of 1 mm particles 

% Heightp1      = [load('data_Z_900K_1mm1','Heightp') load('data_Z_900K_1mm2','Heightp') load('data_Z_900K_1mm3','Heightp') load('data_Z_900K_1mm4','Heightp')...
%     load('data_Z_900K_1mm5','Heightp') load('data_Z_900K_1mm6','Heightp') load('data_Z_900K_1mm7','Heightp')];
% Heightp1      = struct2cell(Heightp1);
% Heightp1      = cell2mat(Heightp1);
% Heightp       = Heightp1(1,:);
% Heightp(1,71) = 0;
% 
% 
% 
% zp1      = [load('data_Z_900K_1mm1','zp') load('data_Z_900K_1mm2','zp') load('data_Z_900K_1mm3','zp') load('data_Z_900K_1mm4','zp')...
%     load('data_Z_900K_1mm5','zp') load('data_Z_900K_1mm6','zp') load('data_Z_900K_1mm7','zp')];
% zp1      = struct2cell(zp1);
% zp1      = cell2mat(zp1);
% zp       = zp1(1,:);


%% Load time scale CFD part 

timestep = 60;

cd 'E:\TUe\Afstuderen\Simulations\Heat_transfer\t_06sec_Ti500k_117m/probes/0/';

data2   = load('T_probes');

Nprobes = length(x1)*length(y)*length(z)+length(x2)*length(y)*length(z)+length(x3)*length(y)*length(z);
check1  = 0;
dt      = 1.0000e-05;


for J = 1:timestep
    check2      = 0;
    check3      = 0;
    
    
    for I = 1:Nprobes
        
        Tf(J,I) = data2(10+check1,2+check3);
        
        check2  = check2+3;
        check3  = check3+1;
        
    end
    t_sim(J,:) = data2(10+check1,1);
    check1      = check1+10;
    
end


%% Calculation Voidfraction/Temperature gas/Velocity gas per cell in the single bubble

cd 'E:\TUe\Afstuderen\Simulations\Heat_transfer\t_06sec_Ti500k_117m/probes/0/';
data3  = load('voidfraction_probes');
data4  = load('U_probes.txt');
data4(:,2:3:length(data4))=[];
data4(:,2:2:length(data4))=[];

dvoid1     = 0;
dvoid2     = 0;
dvoid4     = 0;
dvoid5     = 0;
dvoid6     = 0;
dvoid7     = 0;
dvoid8     = 0;
dvoid9     = 0;
dvoid10    = 0;
dvoid11    = 0;

z     = 0:dL:Heightp(1);

for  ii = 1:(length(z)-1)
    
    for ww = 1:(length(x1)-1)
        Eps_voidx1(ii,ww) = (data3(10,2+dvoid1+dvoid2)+data3(10,3+dvoid1+dvoid2)+data3(10,32+dvoid1+dvoid2)+...
            data3(10,33+dvoid1+dvoid2)+data3(10,17+dvoid1+dvoid2)+data3(10,18+dvoid1+dvoid2)+...
            data3(10,47+dvoid1+dvoid2)+data3(10,48+dvoid1+dvoid2))/8;
        
        T_gasx1(ii,ww) = (data2(10,2+dvoid1+dvoid2)+data2(10,3+dvoid1+dvoid2)+data2(10,32+dvoid1+dvoid2)+...
            data2(10,33+dvoid1+dvoid2)+data2(10,17+dvoid1+dvoid2)+data2(10,18+dvoid1+dvoid2)+...
            data2(10,47+dvoid1+dvoid2)+data2(10,48+dvoid1+dvoid2))/8;
        
        U_gasx1(ii,ww) = (data4(10,2+dvoid1+dvoid2)+data4(10,3+dvoid1+dvoid2)+data4(10,32+dvoid1+dvoid2)+...
            data4(10,33+dvoid1+dvoid2)+data4(10,17+dvoid1+dvoid2)+data4(10,18+dvoid1+dvoid2)+...
            data4(10,47+dvoid1+dvoid2)+data4(10,48+dvoid1+dvoid2))/8;
    end
    dvoid1 = 0;
    dvoid2 = dvoid2 + 30;
    
    for qq = 1:(length(x2)-1)
        Eps_voidx2(ii,qq) = (data3(10,16+dvoid4+dvoid5)+data3(10,(Nprobx1+2)+dvoid4+dvoid7)+data3(10,46+dvoid4+dvoid5)+...
            data3(10,(Nprobx1+6)+dvoid4+dvoid7)+data3(10,31+dvoid4+dvoid5)+data3(10,(Nprobx1+4)+dvoid4+dvoid7)+...
            data3(10,61+dvoid4+dvoid5)+data3(10,(Nprobx1+8)+dvoid4+dvoid7))/8;
        
        
        Eps_voidx3(ii,qq) = (data3(10,(Nprobx1+2)+dvoid6+dvoid7)+data3(10,(Nprobx1+3)+dvoid6+dvoid7)+data3(10,(Nprobx1+6)+dvoid6+dvoid7)+...
            data3(10,(Nprobx1+7)+dvoid6+dvoid7)+data3(10,(Nprobx1+4)+dvoid6+dvoid7)+data3(10,(Nprobx1+5)+dvoid6+dvoid7)+...
            data3(10,(Nprobx1+8)+dvoid6+dvoid7)+data3(10,(Nprobx1+9)+dvoid6+dvoid7))/8;
        
        
        Eps_voidx4(ii,qq) = (data3(10,(Nprobx1+3)+dvoid8+dvoid7)+data3(10,(Nprobx1+Nprobx2+2)+dvoid8+dvoid9)+data3(10,(Nprobx1+7)+dvoid8+dvoid7)+...
            data3(10,(Nprobx1+Nprobx2+40)+dvoid8+dvoid9)+data3(10,(Nprobx1+5)+dvoid8+dvoid7)+data3(10,(Nprobx1+Nprobx2+16)+dvoid8+dvoid9)+...
            data3(10,(Nprobx1+9)+dvoid8+dvoid7)+data3(10,(Nprobx1+Nprobx2+44)+dvoid8+dvoid9))/8;
        
        T_gasx2(ii,qq) = (data2(10,16+dvoid4+dvoid5)+data2(10,(Nprobx1+2)+dvoid4+dvoid7)+data2(10,46+dvoid4+dvoid5)+...
            data2(10,(Nprobx1+6)+dvoid4+dvoid7)+data2(10,31+dvoid4+dvoid5)+data2(10,(Nprobx1+4)+dvoid4+dvoid7)+...
            data2(10,61+dvoid4+dvoid5)+data2(10,(Nprobx1+8)+dvoid4+dvoid7))/8;
        
        
        T_gasx3(ii,qq) = (data2(10,(Nprobx1+2)+dvoid6+dvoid7)+data2(10,(Nprobx1+3)+dvoid6+dvoid7)+data2(10,(Nprobx1+6)+dvoid6+dvoid7)+...
            data2(10,(Nprobx1+7)+dvoid6+dvoid7)+data2(10,(Nprobx1+4)+dvoid6+dvoid7)+data2(10,(Nprobx1+5)+dvoid6+dvoid7)+...
            data2(10,(Nprobx1+8)+dvoid6+dvoid7)+data2(10,(Nprobx1+9)+dvoid6+dvoid7))/8;
        
        
        T_gasx4(ii,qq) = (data2(10,(Nprobx1+3)+dvoid8+dvoid7)+data2(10,(Nprobx1+Nprobx2+2)+dvoid8+dvoid9)+data2(10,(Nprobx1+7)+dvoid8+dvoid7)+...
            data2(10,(Nprobx1+Nprobx2+40)+dvoid8+dvoid9)+data2(10,(Nprobx1+5)+dvoid8+dvoid7)+data2(10,(Nprobx1+Nprobx2+16)+dvoid8+dvoid9)+...
            data2(10,(Nprobx1+9)+dvoid8+dvoid7)+data2(10,(Nprobx1+Nprobx2+44)+dvoid8+dvoid9))/8;
        
        U_gasx2(ii,qq) = (data4(10,16+dvoid4+dvoid5)+data4(10,(Nprobx1+2)+dvoid4+dvoid7)+data4(10,46+dvoid4+dvoid5)+...
            data4(10,(Nprobx1+6)+dvoid4+dvoid7)+data4(10,31+dvoid4+dvoid5)+data4(10,(Nprobx1+4)+dvoid4+dvoid7)+...
            data4(10,61+dvoid4+dvoid5)+data4(10,(Nprobx1+8)+dvoid4+dvoid7))/8;
        
        
        U_gasx3(ii,qq) = (data4(10,(Nprobx1+2)+dvoid6+dvoid7)+data4(10,(Nprobx1+3)+dvoid6+dvoid7)+data4(10,(Nprobx1+6)+dvoid6+dvoid7)+...
            data4(10,(Nprobx1+7)+dvoid6+dvoid7)+data4(10,(Nprobx1+4)+dvoid6+dvoid7)+data4(10,(Nprobx1+5)+dvoid6+dvoid7)+...
            data4(10,(Nprobx1+8)+dvoid6+dvoid7)+data4(10,(Nprobx1+9)+dvoid6+dvoid7))/8;
        
        
        U_gasx4(ii,qq) = (data4(10,(Nprobx1+3)+dvoid8+dvoid7)+data4(10,(Nprobx1+Nprobx2+2)+dvoid8+dvoid9)+data4(10,(Nprobx1+7)+dvoid8+dvoid7)+...
            data4(10,(Nprobx1+Nprobx2+40)+dvoid8+dvoid9)+data4(10,(Nprobx1+5)+dvoid8+dvoid7)+data4(10,(Nprobx1+Nprobx2+16)+dvoid8+dvoid9)+...
            data4(10,(Nprobx1+9)+dvoid8+dvoid7)+data4(10,(Nprobx1+Nprobx2+44)+dvoid8+dvoid9))/8;
        
    end
    dvoid4 = 0;
    dvoid5 = dvoid5+30;
    dvoid6 = 0;
    dvoid7 = dvoid7+4;
    dvoid8 = 0;
    dvoid9 = dvoid9+28;
    
    for rr = 1:(length(x3)-1)
        Eps_voidx5(ii,rr) = (data3(10,(Nprobx1+Nprobx2+2)+dvoid10+dvoid11)+data3(10,(Nprobx1+Nprobx2+3)+dvoid10+dvoid11)+data3(10,(Nprobx1+Nprobx2+30)+dvoid10+dvoid11)+...
            data3(10,(Nprobx1+Nprobx2+31)+dvoid10+dvoid11)+data3(10,(Nprobx1+Nprobx2+16)+dvoid10+dvoid11)+data3(10,(Nprobx1+Nprobx2+17)+dvoid10+dvoid11)+...
            data3(10,(Nprobx1+Nprobx2+44)+dvoid10+dvoid11)+data3(10,(Nprobx1+Nprobx2+45)+dvoid10+dvoid11))/8;
        
        T_gasx5(ii,rr) = (data2(10,(Nprobx1+Nprobx2+2)+dvoid10+dvoid11)+data2(10,(Nprobx1+Nprobx2+3)+dvoid10+dvoid11)+data2(10,(Nprobx1+Nprobx2+30)+dvoid10+dvoid11)+...
            data2(10,(Nprobx1+Nprobx2+31)+dvoid10+dvoid11)+data2(10,(Nprobx1+Nprobx2+16)+dvoid10+dvoid11)+data2(10,(Nprobx1+Nprobx2+17)+dvoid10+dvoid11)+...
            data2(10,(Nprobx1+Nprobx2+44)+dvoid10+dvoid11)+data2(10,(Nprobx1+Nprobx2+45)+dvoid10+dvoid11))/8;
        
        U_gasx5(ii,rr) = (data4(10,(Nprobx1+Nprobx2+2)+dvoid10+dvoid11)+data4(10,(Nprobx1+Nprobx2+3)+dvoid10+dvoid11)+data4(10,(Nprobx1+Nprobx2+30)+dvoid10+dvoid11)+...
            data4(10,(Nprobx1+Nprobx2+31)+dvoid10+dvoid11)+data4(10,(Nprobx1+Nprobx2+16)+dvoid10+dvoid11)+data4(10,(Nprobx1+Nprobx2+17)+dvoid10+dvoid11)+...
            data4(10,(Nprobx1+Nprobx2+44)+dvoid10+dvoid11)+data4(10,(Nprobx1+Nprobx2+45)+dvoid10+dvoid11))/8;
    end
    dvoid10 = 0;
    dvoid11 = dvoid11+28;
    
    
end

dvoid1     = 0;
dvoid2     = 0;
dvoid4     = 0;
dvoid5     = 0;
dvoid6     = 0;
dvoid7     = 0;
dvoid8     = 0;
dvoid9     = 0;
dvoid10    = 0;
dvoid11    = 0;
dvoid3     = 10;

maxp1 = 2;
sum   = length(z);

for q = 1:(length(t_sim)-1)
    
    z         = 0:dL:Heightp(maxp1);
    
    Eps_void1 = zeros((length(z)-1),(length(x1)-1));
    T_gas1    = zeros((length(z)-1),(length(x1)-1));
    U_gas1    = zeros((length(z)-1),(length(x1)-1));
    
    Eps_void2 = zeros((length(z)-1),(length(x2)-1));
    Eps_void3 = zeros((length(z)-1),(length(x2)-1));
    Eps_void4 = zeros((length(z)-1),(length(x2)-1));
    T_gas2 = zeros((length(z)-1),(length(x2)-1));
    T_gas3 = zeros((length(z)-1),(length(x2)-1));
    T_gas4 = zeros((length(z)-1),(length(x2)-1));
    U_gas2 = zeros((length(z)-1),(length(x2)-1));
    U_gas3 = zeros((length(z)-1),(length(x2)-1));
    U_gas4 = zeros((length(z)-1),(length(x2)-1));
    
    Eps_void5 = zeros((length(z)-1),(length(x3)-1));
    T_gas5    = zeros((length(z)-1),(length(x3)-1));
    U_gas5    = zeros((length(z)-1),(length(x3)-1));
    
    for  i = 1:(length(z)-1)
        
        for w = 1:(length(x1)-1)
            
            Eps_void1(i,w) = (data3(10+dvoid3,2+dvoid1+dvoid2)+data3(10+dvoid3,3+dvoid1+dvoid2)+...
                data3(10+dvoid3,32+dvoid1+dvoid2)+data3(10+dvoid3,33+dvoid1+dvoid2)+data3(10+dvoid3,17+dvoid1+dvoid2)+...
                data3(10+dvoid3,18+dvoid1+dvoid2)+data3(10+dvoid3,47+dvoid1+dvoid2)+data3(10+dvoid3,48+dvoid1+dvoid2))/8;
            
            T_gas1(i,w) = (data2(10+dvoid3,2+dvoid1+dvoid2)+data2(10+dvoid3,3+dvoid1+dvoid2)+...
                data2(10+dvoid3,32+dvoid1+dvoid2)+data2(10+dvoid3,33+dvoid1+dvoid2)+data2(10+dvoid3,17+dvoid1+dvoid2)+...
                data2(10+dvoid3,18+dvoid1+dvoid2)+data2(10+dvoid3,47+dvoid1+dvoid2)+data2(10+dvoid3,48+dvoid1+dvoid2))/8;
            
            U_gas1(i,w) = (data4(10+dvoid3,2+dvoid1+dvoid2)+data4(10+dvoid3,3+dvoid1+dvoid2)+...
                data4(10+dvoid3,32+dvoid1+dvoid2)+data4(10+dvoid3,33+dvoid1+dvoid2)+data4(10+dvoid3,17+dvoid1+dvoid2)+...
                data4(10+dvoid3,18+dvoid1+dvoid2)+data4(10+dvoid3,47+dvoid1+dvoid2)+data4(10+dvoid3,48+dvoid1+dvoid2))/8;
            
            dvoid1 = dvoid1+1;
            
        end
        dvoid1 = 0;
        dvoid2 = dvoid2 + 30;
        
        for a = 1:(length(x2)-1)
            
            Eps_void2(i,a) = (data3(10+dvoid3,16+dvoid4+dvoid5)+data3(10+dvoid3,(Nprobx1+2)+dvoid4+dvoid7)+data3(10+dvoid3,46+dvoid4+dvoid5)+...
                data3(10+dvoid3,(Nprobx1+6)+dvoid4+dvoid7)+data3(10+dvoid3,31+dvoid4+dvoid5)+data3(10+dvoid3,(Nprobx1+4)+dvoid4+dvoid7)+...
                data3(10+dvoid3,61+dvoid4+dvoid5)+data3(10+dvoid3,(Nprobx1+8)+dvoid4+dvoid7))/8;
            
            Eps_void3(i,a) = (data3(10+dvoid3,(Nprobx1+2)+dvoid6+dvoid7)+data3(10+dvoid3,(Nprobx1+3)+dvoid6+dvoid7)+data3(10+dvoid3,(Nprobx1+6)+dvoid6+dvoid7)+...
                data3(10+dvoid3,(Nprobx1+7)+dvoid6+dvoid7)+data3(10+dvoid3,(Nprobx1+4)+dvoid6+dvoid7)+data3(10+dvoid3,(Nprobx1+5)+dvoid6+dvoid7)+...
                data3(10+dvoid3,(Nprobx1+8)+dvoid6+dvoid7)+data3(10+dvoid3,(Nprobx1+9)+dvoid6+dvoid7))/8;
            
            Eps_void4(i,a) = (data3(10+dvoid3,(Nprobx1+3)+dvoid8+dvoid7)+data3(10+dvoid3,(Nprobx1+Nprobx2+2)+dvoid8+dvoid9)+data3(10+dvoid3,(Nprobx1+7)+dvoid8+dvoid7)+...
                data3(10+dvoid3,(Nprobx1+Nprobx2+40)+dvoid8+dvoid9)+data3(10+dvoid3,(Nprobx1+5)+dvoid8+dvoid7)+data3(10+dvoid3,(Nprobx1+Nprobx2+16)+dvoid8+dvoid9)+...
                data3(10+dvoid3,(Nprobx1+9)+dvoid8+dvoid7)+data3(10+dvoid3,(Nprobx1+Nprobx2+44)+dvoid8+dvoid9))/8;
            
            T_gas2(i,a) = (data2(10+dvoid3,16+dvoid4+dvoid5)+data2(10+dvoid3,(Nprobx1+2)+dvoid4+dvoid7)+data2(10+dvoid3,46+dvoid4+dvoid5)+...
                data2(10+dvoid3,(Nprobx1+6)+dvoid4+dvoid7)+data2(10+dvoid3,31+dvoid4+dvoid5)+data2(10+dvoid3,(Nprobx1+4)+dvoid4+dvoid7)+...
                data2(10+dvoid3,61+dvoid4+dvoid5)+data2(10+dvoid3,(Nprobx1+8)+dvoid4+dvoid7))/8;
            
            T_gas3(i,a) = (data2(10+dvoid3,(Nprobx1+2)+dvoid6+dvoid7)+data2(10+dvoid3,(Nprobx1+3)+dvoid6+dvoid7)+data2(10+dvoid3,(Nprobx1+6)+dvoid6+dvoid7)+...
                data2(10+dvoid3,(Nprobx1+7)+dvoid6+dvoid7)+data2(10+dvoid3,(Nprobx1+4)+dvoid6+dvoid7)+data2(10+dvoid3,(Nprobx1+5)+dvoid6+dvoid7)+...
                data2(10+dvoid3,(Nprobx1+8)+dvoid6+dvoid7)+data2(10+dvoid3,(Nprobx1+9)+dvoid6+dvoid7))/8;
            
            T_gas4(i,a) = (data2(10+dvoid3,(Nprobx1+3)+dvoid8+dvoid7)+data2(10+dvoid3,(Nprobx1+Nprobx2+2)+dvoid8+dvoid9)+data2(10+dvoid3,(Nprobx1+7)+dvoid8+dvoid7)+...
                data2(10+dvoid3,(Nprobx1+Nprobx2+40)+dvoid8+dvoid9)+data2(10+dvoid3,(Nprobx1+5)+dvoid8+dvoid7)+data2(10+dvoid3,(Nprobx1+Nprobx2+16)+dvoid8+dvoid9)+...
                data2(10+dvoid3,(Nprobx1+9)+dvoid8+dvoid7)+data2(10+dvoid3,(Nprobx1+Nprobx2+44)+dvoid8+dvoid9))/8;
            
            U_gas2(i,a) = (data4(10+dvoid3,16+dvoid4+dvoid5)+data4(10+dvoid3,(Nprobx1+2)+dvoid4+dvoid7)+data4(10+dvoid3,46+dvoid4+dvoid5)+...
                data4(10+dvoid3,(Nprobx1+6)+dvoid4+dvoid7)+data4(10+dvoid3,31+dvoid4+dvoid5)+data4(10+dvoid3,(Nprobx1+4)+dvoid4+dvoid7)+...
                data4(10+dvoid3,61+dvoid4+dvoid5)+data4(10+dvoid3,(Nprobx1+8)+dvoid4+dvoid7))/8;
            
            U_gas3(i,a) = (data4(10+dvoid3,(Nprobx1+2)+dvoid6+dvoid7)+data4(10+dvoid3,(Nprobx1+3)+dvoid6+dvoid7)+data4(10+dvoid3,(Nprobx1+6)+dvoid6+dvoid7)+...
                data4(10+dvoid3,(Nprobx1+7)+dvoid6+dvoid7)+data4(10+dvoid3,(Nprobx1+4)+dvoid6+dvoid7)+data4(10+dvoid3,(Nprobx1+5)+dvoid6+dvoid7)+...
                data4(10+dvoid3,(Nprobx1+8)+dvoid6+dvoid7)+data4(10+dvoid3,(Nprobx1+9)+dvoid6+dvoid7))/8;
            
            U_gas4(i,a) = (data4(10+dvoid3,(Nprobx1+3)+dvoid8+dvoid7)+data4(10+dvoid3,(Nprobx1+Nprobx2+2)+dvoid8+dvoid9)+data4(10+dvoid3,(Nprobx1+7)+dvoid8+dvoid7)+...
                data4(10+dvoid3,(Nprobx1+Nprobx2+40)+dvoid8+dvoid9)+data4(10+dvoid3,(Nprobx1+5)+dvoid8+dvoid7)+data4(10+dvoid3,(Nprobx1+Nprobx2+16)+dvoid8+dvoid9)+...
                data4(10+dvoid3,(Nprobx1+9)+dvoid8+dvoid7)+data4(10+dvoid3,(Nprobx1+Nprobx2+44)+dvoid8+dvoid9))/8;
            
            dvoid4 = dvoid4+1;
            dvoid6 = dvoid6+1;
            dvoid8 = dvoid8+1;
        end
        dvoid4 = 0;
        dvoid5 = dvoid5+30;
        dvoid6 = 0;
        dvoid7 = dvoid7+4;
        dvoid8 = 0;
        dvoid9 = dvoid9+28;
        
        
        for r = 1:(length(x3)-1)
            Eps_void5(i,r) = (data3(10+dvoid3,(Nprobx1+Nprobx2+2)+dvoid10+dvoid11)+data3(10+dvoid3,(Nprobx1+Nprobx2+3)+dvoid10+dvoid11)+data3(10+dvoid3,(Nprobx1+Nprobx2+30)+dvoid10+dvoid11)+...
                data3(10+dvoid3,(Nprobx1+Nprobx2+31)+dvoid10+dvoid11)+data3(10+dvoid3,(Nprobx1+Nprobx2+16)+dvoid10+dvoid11)+data3(10+dvoid3,(Nprobx1+Nprobx2+17)+dvoid10+dvoid11)+...
                data3(10+dvoid3,(Nprobx1+Nprobx2+44)+dvoid10+dvoid11)+data3(10+dvoid3,(Nprobx1+Nprobx2+45)+dvoid10+dvoid11))/8;
            
            T_gas5(i,r) = (data2(10+dvoid3,(Nprobx1+Nprobx2+2)+dvoid10+dvoid11)+data2(10+dvoid3,(Nprobx1+Nprobx2+3)+dvoid10+dvoid11)+data2(10+dvoid3,(Nprobx1+Nprobx2+30)+dvoid10+dvoid11)+...
                data2(10+dvoid3,(Nprobx1+Nprobx2+31)+dvoid10+dvoid11)+data2(10+dvoid3,(Nprobx1+Nprobx2+16)+dvoid10+dvoid11)+data2(10+dvoid3,(Nprobx1+Nprobx2+17)+dvoid10+dvoid11)+...
                data2(10+dvoid3,(Nprobx1+Nprobx2+44)+dvoid10+dvoid11)+data2(10+dvoid3,(Nprobx1+Nprobx2+45)+dvoid10+dvoid11))/8;
            
            U_gas5(i,r) = (data4(10+dvoid3,(Nprobx1+Nprobx2+2)+dvoid10+dvoid11)+data4(10+dvoid3,(Nprobx1+Nprobx2+3)+dvoid10+dvoid11)+data4(10+dvoid3,(Nprobx1+Nprobx2+30)+dvoid10+dvoid11)+...
                data4(10+dvoid3,(Nprobx1+Nprobx2+31)+dvoid10+dvoid11)+data4(10+dvoid3,(Nprobx1+Nprobx2+16)+dvoid10+dvoid11)+data4(10+dvoid3,(Nprobx1+Nprobx2+17)+dvoid10+dvoid11)+...
                data4(10+dvoid3,(Nprobx1+Nprobx2+44)+dvoid10+dvoid11)+data4(10+dvoid3,(Nprobx1+Nprobx2+45)+dvoid10+dvoid11))/8;
            
            dvoid10 = dvoid10+1;
        end
        dvoid10 = 0;
        dvoid11 = dvoid11+28;
        
        
    end
    dvoid2     = 0;
    dvoid5     = 0;
    dvoid7     = 0;
    dvoid9     = 0;
    dvoid11    = 0;
    dvoid3     = dvoid3+10;
    Eps_voidx1 = [Eps_voidx1; Eps_void1];
    Eps_voidx2 = [Eps_voidx2; Eps_void2];
    Eps_voidx3 = [Eps_voidx3; Eps_void3];
    Eps_voidx4 = [Eps_voidx4; Eps_void4];
    Eps_voidx5 = [Eps_voidx5; Eps_void5];
    T_gasx1    = [T_gasx1; T_gas1];
    T_gasx2    = [T_gasx2; T_gas2];
    T_gasx3    = [T_gasx3; T_gas3];
    T_gasx4    = [T_gasx4; T_gas4];
    T_gasx5    = [T_gasx5; T_gas5];
    U_gasx1    = [U_gasx1; U_gas1];
    U_gasx2    = [U_gasx2; U_gas2];
    U_gasx3    = [U_gasx3; U_gas3];
    U_gasx4    = [U_gasx4; U_gas4];
    U_gasx5    = [U_gasx5; U_gas5];
    
    sum  = sum+(length(z)-1);
    maxp1 = maxp1+1;
    
    
end

Voidfraction = [Eps_voidx1 Eps_voidx2 Eps_voidx3 Eps_voidx4 Eps_voidx5];
Temp_gas     = [T_gasx1 T_gasx2 T_gasx3 T_gasx4 T_gasx5];
Vel_gas      = [U_gasx1 U_gasx2 U_gasx3 U_gasx4 U_gasx5];

%% Calculation Volume bubble and diameter

fraction = 0;
Volumebubble = zeros(length(t_sim),1);

V_bub    = zeros(length(t_sim),1);
V_bub2   = zeros(length(t_sim),1);
V_bub3   = zeros(length(t_sim),1);
V_solid  = zeros(length(t_sim),1);
T_bub    = zeros(length(t_sim),1);
Vel_bub  = zeros(length(t_sim),1);
T_track  = zeros(length(t_sim),1);
V_trackz = zeros(length(t_sim),Ncell_x);
V_trackx = zeros(length(t_sim),(max(zp)-1));

maxp2 = 1;


for i = 1:length(t_sim)
    z        = 0:dL:Heightp(maxp2);
    steptest = length(z);
    
    for v = 1:(length(z)-1)
        for j = 1:Ncell_x
            
            if Voidfraction(v+fraction,j) >= 0.75
                
                V_bub(i,1)    = V_bub(i,1) + (Vcell*Voidfraction(v+fraction,j));
                V_bub2(i,1)   = V_bub2(i,1) + (Vcell*Voidfraction(v+fraction,j))+(Vcell*(1-Voidfraction(v+fraction,j)));
                V_bub3(i,1)   = V_bub3(i,1) + Vcell;
                V_solid(i,1)  = V_solid(i,1) + (Vcell*(1-Voidfraction(v+fraction,j)));
                T_bub(i,1)    = T_bub(i,1) + Temp_gas(v+fraction,j);
                Vel_bub(i,1)  = Vel_bub(i,1) + Vel_gas(v+fraction,j);
                T_track(i,1)  = T_track(i,1)+1;
                V_trackz(i,j) = V_trackz(i,j)+1;
                V_trackx(i,v) = V_trackx(i,v)+1;
                
            else
                V_bub(i,1)    = V_bub(i,1);
                V_bub2(i,1)   = V_bub2(i,1);
                V_bub3(i,1)   = V_bub3(i,1);
                V_solid(i,1)  = V_solid(i,1);
                T_bub(i,1)    = T_bub(i,1);
                Vel_bub(i,1)  = Vel_bub(i,1);
                T_track(i,1)  = T_track(i,1);
                V_trackz(i,j) = V_trackz(i,j);
                V_trackx(i,v) = V_trackx(i,v);
            end
        end
        
    end
    
    fraction = fraction+(steptest-1);
    maxp2    = maxp2+1;
    
    
end


%%
T_bub   = T_bub./T_track;
Vel_bub = Vel_bub./T_track;

DIAMz    = V_trackz.*dL;
Dia_bubz = (DIAMz(:,15)+DIAMz(:,16))/2;

DIAMx    = V_trackx.*dW;
Dia_bubx = max(DIAMx,[],2);

A_dia = pi.*Dia_bubz.*Dia_bubx./4;
P_dia = 2*pi*0.5*((Dia_bubz./2).^2+(Dia_bubx./2).^2).^0.5;
d_bub = (1.55*(A_dia).^0.625)./((P_dia).^0.25);


% ZERO = 31;   % Use for 1 mm particles
ZERO = 21;   % Use for 2 mm particles

VOLUMEBUB = pi*((Dia_bubx./2).^2)*B;

%% Heat transfer Davidson & Harrison (three diameters, x-/z- and equivalent)

Ubg = 0.6; 


h_bub1     = ((2.*Ubg.*rho_g*Cp_g)./pi)+0.6*((k_g*rho_g*Cp_g)^(0.5)*g^(0.25))./(Dia_bubz(ZERO:length(t_sim)).^(0.25));
h_bub2     = ((2.*Ubg.*rho_g*Cp_g)./pi)+0.6*((k_g*rho_g*Cp_g)^(0.5)*g^(0.25))./(Dia_bubx(ZERO:length(t_sim)).^(0.25));
h_bub3     = ((2.*Ubg.*rho_g*Cp_g)./pi)+0.6*((k_g*rho_g*Cp_g)^(0.5)*g^(0.25))./(d_bub(ZERO:length(t_sim)).^(0.25));

h_bub      = [h_bub1 h_bub2 h_bub3];

%% Heat transfer Simulation (three diameters, x-/z- and equivalent)

ZERO1  = find(T_bub==max(T_bub)); % calculation of t(0)

A_bub1 = (2*pi.*(Dia_bubz/2).*B); 
A_bub2 = (2*pi.*(Dia_bubx/2).*B);
A_bub3 = 2*pi*B.*(sqrt(0.5.*(((Dia_bubx./2).^2)+((Dia_bubz./2).^2))));

T_dim = (T_bub-Te)./(T_bub(ZERO)-Te);
LOG = -log(T_dim(ZERO:length(t_sim)));

h_sim1 = (LOG.*(rho_g.*V_bub(ZERO:length(t_sim)).*Cp_g))./(A_bub1(ZERO:length(t_sim)).*(t_sim(ZERO:length(t_sim))-t_sim(ZERO)));
h_sim2 = (LOG.*(rho_g.*V_bub(ZERO:length(t_sim)).*Cp_g))./(A_bub2(ZERO:length(t_sim)).*(t_sim(ZERO:length(t_sim))-t_sim(ZERO)));
h_sim3 = (LOG.*(rho_g.*V_bub(ZERO:length(t_sim)).*Cp_g))./(A_bub3(ZERO:length(t_sim)).*(t_sim(ZERO:length(t_sim))-t_sim(ZERO)));


%% Heat transfer Total Bed Levenspiel & Kunii (see Appendix master thesis)

Gammab = V_solid./(V_bub+V_solid);
U_br   = 0.711.*((g.*d_bub(ZERO:length(t_sim))).^0.5);
U_0    = 2.2;
U_mf   = U_0*eps_mf;

Re_p = (dp.*U_0.*rho_g)./mu;             % Reynolds number
Pr   = mu*Cp_g/k_g;                      % Prandlt number
Pe   = Re_p.*Pr;                         % Peclet number
Nu_p = 2+0.6.*(Re_p.^(0.5)).*Pr^(1/3);   % Nusselt at surface particle

H_bc = 4.5*((U_mf*rho_g*Cp_g)./d_bub(ZERO:length(t_sim))+5.85*(((k_g*rho_g*Cp_g)^0.5*(g.^0.25))./(d_bub(ZERO:length(t_sim))).^(5/4)));

Partial = (U_0 - U_mf)./(U_br.*(1-eps_mf));

Nu_bed = Partial.*((Gammab(ZERO:length(t_sim)).*Nu_p*eta_h)+((phi*dp^2)/(6*k_g)).*H_bc);

h_bed = (Nu_bed.*k_g)./dp;

%% Calculation hBE (Benai et al., see Appendix master thesis)
%%% Check for linearity

Constant = (LOG.*V_bub(ZERO:length(t_sim)))./A_bub3(ZERO:length(t_sim));

t_t   = t_sim(ZERO:length(t_sim))-t_sim(ZERO);
Line  = polyfit(t_t,Constant,3);
Line1 = polyval(Line,t_t);
figure
hold on
plot(t_t,Constant,'.');
plot(t_t,Line1);



%% Calculation Gamma hBE
close all

TT     = (T_bub(ZERO:length(t_sim))-Te);
Line2  = polyfit(log(t_t(2:length(t_t))),log(TT(2:length(t_t))),1);
Line21  = polyfit(t_t,TT,3);
Line3  = exp(Line2(2)).*t_t.^Line2(1);
Line31  = polyval(Line21,t_t);
figure
hold on
plot(t_t,TT,'.');
plot(t_t,Line3);
xlabel('(t-t_{inj}) [s]','FontSize',14)
ylabel('(Tb-Te) [K]','FontSize',14)
filesave='E:\TUe\Afstuderen\Master_Thesis\images\gammalog1_500K.png';
saveas(gcf,filesave)

figure
hold on
plot(t_t,TT,'.');
plot(t_t,Line31);
xlabel('(t-t_{inj}) [s]','FontSize',14)
ylabel('(Tb-Te) [K]','FontSize',14)
filesave='E:\TUe\Afstuderen\Master_Thesis\images\gammalog2_500K.png';
saveas(gcf,filesave)

corr = abs(-log(Line3./(TT))./t_t);
A_av = A_bub3(ZERO+length(t_t)/2);
m_av = rho_g*V_bub(ZERO+length(t_t)/2);

Line4 = (corr.*m_av.*Cp_g)./A_av;
Line5 = polyfit(t_t(2:length(t_t)),Line4(2:length(t_t)),3);
HBE   = polyval(Line5,t_t(2:length(t_t)));

corr1 = abs(-log(Line31./(TT))./t_t);

Line41 = (corr1.*rho_g.*V_bub(ZERO:length(t_sim)).*Cp_g)./A_bub3(ZERO:length(t_sim));
Line51 = polyfit(t_t(2:length(t_t)),Line41(2:length(t_t)),3);
HBE1   = polyval(Line51,t_t(2:length(t_t)));

figure
hold on
plot(t_t(2:length(t_t)),HBE,'x-')
plot(t_t,h_bed,'o-')
plot(t_t,h_bub3,'square-')
plot(t_t,h_sim3,'v-')
ylim([-100 1500])
legend('HBE','h_{bed}','h_{DH}','h_{sim}')
filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_baneai500.png';
saveas(gcf,filesave)

figure
hold on
plot(t_t(2:length(t_t)),HBE,'x-')
plot(t_t(2:length(t_t)),HBE1,'O-')
plot(t_t,h_bub3,'square-')
plot(t_t,h_sim3,'v-')
ylim([-100 1500])
legend('HBE','HBE1','h_{DH}','h_{sim}')
filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_baneai2500.png';
saveas(gcf,filesave)

%% Calculation position bubble

Bub  = ones(1,Ncell_x);
step = 0;

maxp3 = 1;
z     = 0:dL:Heightp(maxp3);

for ij = 1:length(t_sim)
    for in = 1:(length(z)-1)
        for n = 1:Ncell_x
            
            if Voidfraction(in+step,n) >= 0.8
                
                Bub1(in,n) = Voidfraction(in+step,n);
                
            else
                Bub1(in,n) = 0;
                
            end
        end
        
    end
    
    Bub   = [Bub; Bub1];
    step  = step+(length(z)-1);
    maxp3 = maxp3+1;
    z     = 0:dL:Heightp(maxp3);
end

Bub1 = Bub(2+ZERO1*(length(z)-1):length(Voidfraction)+1,1:Ncell_x/2);
Bub2 = Bub(2+ZERO1*(length(z)-1):length(Voidfraction)+1,(Ncell_x/2+1):Ncell_x);


%% Bubble tracking
Pos1 = zeros(length(Bub1),1);
Bub1(Bub1==0)=2;
Bub2(Bub2==0)=2;

for jj = 1:length(Bub1)
    
    if Bub1(jj,Ncell_x/2) > 0 && Bub1(jj,Ncell_x/2) < 2
        
        minPos1    = min(Bub1(jj,:),[],2);
        Pos1(jj,1) = find(Bub1(jj,:)==minPos1);
        
    else
        Pos1(jj,1) =NaN;
        
    end
    
    if Bub2(jj,1) > 0 && Bub2(jj,1) < 2
        
        minPos2    = min(Bub2(jj,:),[],2);
        Pos2(jj,1) = find(Bub2(jj,:)==minPos2);
        
    else
        Pos2(jj,1) =NaN;
    end
    
end

LocBub1 = (Pos1-1).*dW;
LocBub2 = (Pos2+Ncell_x/2).*dW;



%% Plot bubble movement and create GIF

cd E:\TUe\Afstuderen\Master_Thesis\images

h = figure;
axis tight manual % this ensures that getframe() returns a consistent size
filename = 't_07sec_Ti900k_117m_1mm.gif';
step = 0;

for qq = 1:length(Bub1)/(length(z)-1)

    AXX1 = LocBub1(1+step:(length(z)-1)+step);
    AXX2 = LocBub2(1+step:(length(z)-1)+step);

    plot(AXX1,z(1:length(z)-1),'.',AXX2,z(1:length(z)-1),'.')
    axis([0 W 0 0.75*L])
    xlabel('x-axis [m]','FontSize',14)
    ylabel('z-axis [m]','FontSize',14)

    %%% Capture the plot as an image
    frame = getframe(h);

    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);

    %%% Write to the GIF File
    if qq == 1
        imwrite(imind,cm,filename,'gif','Loopcount',inf);
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append');
    end

    step = step + (length(z)-1);

end

close all

%% Save data heat transfer

cd E:\TUe\Afstuderen\Simulations\Heat_transfer
save('t06sec_Ti500K_117m','ZERO','ZERO1','t_sim','Dia_bubz','Dia_bubx','d_bub','T_bub','h_bub','T_dim','h_sim1','h_sim2','h_sim3','LocBub1','LocBub2','h_bed','V_bub')

%% Ramp inlet plot (different set ups inlet velocity)

t_in1 = [0 0.1 0.2 0.3 0.31 0.6];
t_in2 = [0 0.1 0.15 0.6];
u_in1 = [0 0 30 30 1.1 1.1];
u_in2 = [0 0 1.1 1.1];
T_in1 = [300 300 900 900 300 300];
T_in2 = [300 300 300 300];

box on
subplot(1,2,1)
grid on
hold on
plot(t_in1,u_in1,'LineWidth',2)
plot(t_in2,u_in2,'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Inlet velocity (m/s)','FontSize',14)
ylim([0 35])
xlim([0 0.6])

subplot(1,2,2)
grid on 
hold on
plot(t_in1,T_in1,'LineWidth',2)
plot(t_in2,T_in2,'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Inlet temperature (K)','FontSize',14)
ylim([0 1000])
xlim([0 0.6])
legend('Jet','Background','Location','southeast')
filesave='E:\TUe\Afstuderen\Master_Thesis\images\rampinlet.png';
saveas(gcf,filesave)



