%% This matlab script is used to plot different properties of the simulations to validate the heat transfer in a single bubble

clear all; close all; clc;

%% Validation Heat transfer bubble

cd G:\Heat_transfer                                     % Right path to the simulation properties

t_sim      = load('t06sec_Ti500K_117m','t_sim');        % Time of the simulation of 2 mm particles
t_sim      = struct2cell(t_sim);
t_sim      = cell2mat(t_sim);

t_sim1mm      = load('t07sec_Ti900K_117m_1mm','t_sim'); % Time of the simulation of 2 mm particles
t_sim1mm      = struct2cell(t_sim1mm);
t_sim1mm      = cell2mat(t_sim1mm);

h_bed = [load('t06sec_Ti900K_117m','h_bed') load('t06sec_Ti700K_117m','h_bed') load('t06sec_Ti500K_117m','h_bed')];
h_bed = struct2cell(h_bed);
h_bed = cell2mat(h_bed);

h_bed1mm = [load('t07sec_Ti900K_117m_1mm','h_bed')];
h_bed1mm = struct2cell(h_bed1mm);
h_bed1mm = cell2mat(h_bed1mm);

T_bub = [load('t06sec_Ti900K_117m','T_bub') load('t06sec_Ti700K_117m','T_bub') load('t06sec_Ti500K_117m','T_bub')];
T_bub = struct2cell(T_bub);
T_bub = cell2mat(T_bub);

T_bub1mm = [load('t07sec_Ti900K_117m_1mm','T_bub')];
T_bub1mm = struct2cell(T_bub1mm);
T_bub1mm = cell2mat(T_bub1mm);

V_bub = [load('t06sec_Ti900K_117m','V_bub') load('t06sec_Ti700K_117m','V_bub') load('t06sec_Ti500K_117m','V_bub')];
V_bub = struct2cell(V_bub);
V_bub = cell2mat(V_bub);

V_bub1mm = [load('t07sec_Ti900K_117m_1mm','V_bub')];
V_bub1mm = struct2cell(V_bub1mm);
V_bub1mm = cell2mat(V_bub1mm);

d_bub = [load('t06sec_Ti900K_117m','d_bub') load('t06sec_Ti700K_117m','d_bub') load('t06sec_Ti500K_117m','d_bub')];
d_bub = struct2cell(d_bub);
d_bub = cell2mat(d_bub);

d_bub1mm = [load('t07sec_Ti900K_117m_1mm','d_bub')];
d_bub1mm = struct2cell(d_bub1mm);
d_bub1mm = cell2mat(d_bub1mm);

Dia_bubx = [load('t06sec_Ti900K_117m','Dia_bubx') load('t06sec_Ti700K_117m','Dia_bubx') load('t06sec_Ti500K_117m','Dia_bubx')];
Dia_bubx = struct2cell(Dia_bubx);
Dia_bubx = cell2mat(Dia_bubx);

Dia_bubx1mm = [load('t07sec_Ti900K_117m_1mm','Dia_bubx')];
Dia_bubx1mm = struct2cell(Dia_bubx1mm);
Dia_bubx1mm = cell2mat(Dia_bubx1mm);

Dia_bubz = [load('t06sec_Ti900K_117m','Dia_bubz') load('t06sec_Ti700K_117m','Dia_bubz') load('t06sec_Ti500K_117m','Dia_bubz')];
Dia_bubz = struct2cell(Dia_bubz);
Dia_bubz = cell2mat(Dia_bubz);

Dia_bubz1mm = [load('t07sec_Ti900K_117m_1mm','Dia_bubz')];
Dia_bubz1mm = struct2cell(Dia_bubz1mm);
Dia_bubz1mm = cell2mat(Dia_bubz1mm);

T_dim = [load('t06sec_Ti900K_117m','T_dim') load('t06sec_Ti700K_117m','T_dim') load('t06sec_Ti500K_117m','T_dim')];
T_dim = struct2cell(T_dim);
T_dim = cell2mat(T_dim);

T_dim1mm = [load('t07sec_Ti900K_117m_1mm','T_dim')];
T_dim1mm = struct2cell(T_dim1mm);
T_dim1mm = cell2mat(T_dim1mm);

ZERO1mm = load('t07sec_Ti900K_117m_1mm','ZERO');
ZERO1mm = struct2cell(ZERO1mm);
ZERO1mm = cell2mat(ZERO1mm);

ZERO1 = load('t06sec_Ti900K_117m','ZERO');
ZERO1 = struct2cell(ZERO1);
ZERO1 = cell2mat(ZERO1);

ZERO2 = load('t06sec_Ti700K_117m','ZERO');
ZERO2 = struct2cell(ZERO2);
ZERO2 = cell2mat(ZERO2);

ZERO3 = load('t06sec_Ti500K_117m','ZERO');
ZERO3 = struct2cell(ZERO3);
ZERO3 = cell2mat(ZERO3);

ZERO11mm = load('t07sec_Ti900K_117m_1mm','ZERO1');
ZERO11mm = struct2cell(ZERO11mm);
ZERO11mm = cell2mat(ZERO11mm);

ZERO11 = load('t06sec_Ti900K_117m','ZERO1');
ZERO11 = struct2cell(ZERO11);
ZERO11 = cell2mat(ZERO11);

ZERO12 = load('t06sec_Ti700K_117m','ZERO1');
ZERO12 = struct2cell(ZERO12);
ZERO12 = cell2mat(ZERO12);

ZERO13 = load('t06sec_Ti500K_117m','ZERO1');
ZERO13 = struct2cell(ZERO13);
ZERO13 = cell2mat(ZERO13);

h_bub = [load('t06sec_Ti900K_117m','h_bub') load('t06sec_Ti700K_117m','h_bub') load('t06sec_Ti500K_117m','h_bub')];
h_bub = struct2cell(h_bub);
h_bub = cell2mat(h_bub);

h_bub1mm = [load('t07sec_Ti900K_117m_1mm','h_bub')];
h_bub1mm = struct2cell(h_bub1mm);
h_bub1mm = cell2mat(h_bub1mm);
h_bub11mm = h_bub1mm(:,1);
h_bub21mm = h_bub1mm(:,2);
h_bub31mm = h_bub1mm(:,3);

h_bub1 = h_bub(:,:,1);
h_bub2 = h_bub(:,:,2);
h_bub3 = h_bub(:,:,3);

h_bub11 = h_bub1(:,1);
h_bub12 = h_bub2(:,1);
h_bub13 = h_bub3(:,1);

h_bub21 = h_bub1(:,2);
h_bub22 = h_bub2(:,2);
h_bub23 = h_bub3(:,2);

h_bub31 = h_bub1(:,3);
h_bub32 = h_bub2(:,3);
h_bub33 = h_bub3(:,3);

h_sim1 = [load('t06sec_Ti900K_117m','h_sim1') load('t06sec_Ti700K_117m','h_sim1') load('t06sec_Ti500K_117m','h_sim1')];
h_sim1 = [struct2cell(h_sim1(1)) struct2cell(h_sim1(2)) struct2cell(h_sim1(3))];
h_sim11 = cell2mat(h_sim1(1));
h_sim12 = cell2mat(h_sim1(2));
h_sim13 = cell2mat(h_sim1(3));

h_sim11mm = [load('t07sec_Ti900K_117m_1mm','h_sim1')];
h_sim11mm = [struct2cell(h_sim11mm)];
h_sim11mm = cell2mat(h_sim11mm);

h_sim2 = [load('t06sec_Ti900K_117m','h_sim2') load('t06sec_Ti700K_117m','h_sim2') load('t06sec_Ti500K_117m','h_sim2')];
h_sim2 = [struct2cell(h_sim2(1)) struct2cell(h_sim2(2)) struct2cell(h_sim2(3))];
h_sim21 = cell2mat(h_sim2(1));
h_sim22 = cell2mat(h_sim2(2));
h_sim23 = cell2mat(h_sim2(3));

h_sim21mm = [load('t07sec_Ti900K_117m_1mm','h_sim2')];
h_sim21mm = [struct2cell(h_sim21mm)];
h_sim21mm = cell2mat(h_sim21mm);

h_sim3 = [load('t06sec_Ti900K_117m','h_sim3') load('t06sec_Ti700K_117m','h_sim3') load('t06sec_Ti500K_117m','h_sim3')];
h_sim3 = [struct2cell(h_sim3(1)) struct2cell(h_sim3(2)) struct2cell(h_sim3(3))];
h_sim31 = cell2mat(h_sim3(1));
h_sim32 = cell2mat(h_sim3(2));
h_sim33 = cell2mat(h_sim3(3));

h_sim31mm = [load('t07sec_Ti900K_117m_1mm','h_sim3')];
h_sim31mm = [struct2cell(h_sim31mm)];
h_sim31mm = cell2mat(h_sim31mm);

HEAT_900 = [h_sim11 h_sim21 h_sim31 h_bub11 h_bub21 h_bub31];
HEAT_700 = [h_sim12 h_sim22 h_sim32 h_bub12 h_bub22 h_bub32];
HEAT_500 = [h_sim13 h_sim23 h_sim33 h_bub13 h_bub23 h_bub33];

tt2mm = t_sim(ZERO1:length(t_sim))-t_sim(ZERO1);
tt1mm = t_sim1mm(ZERO11mm:length(t_sim1mm))-t_sim(ZERO11mm);

%% Plot data Bubble validation
close all

%% Bubble equivalent diameter 2 mm
figure(1)
box on
grid on
hold on
plot(t_sim(10:50),d_bub((10:50),1),'LineWidth',2)
plot(t_sim(10:45),d_bub((10:45),2),'LineWidth',2)
plot(t_sim(10:40),d_bub((10:40),3),'LineWidth',2)
xlabel('Time s','FontSize',14)
ylabel('Equivalent bubble diameter (m)','FontSize',14)
v=legend('T_i = 900K','T_i = 700K','T_i = 500K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblediameterequivalent.png';
% saveas(gcf,filesave)

%% Bubble diameter x-direction 2 mm
figure(2)
box on
grid on
hold on
plot(t_sim(10:50),Dia_bubx((10:50),1),'LineWidth',2)
plot(t_sim(10:45),Dia_bubx((10:45),2),'LineWidth',2)
plot(t_sim(10:40),Dia_bubx((10:40),3),'LineWidth',2)
xlabel('Time [s]','FontSize',14)
ylabel('Bubble diameter x-direction (m)','FontSize',14)
v=legend('T_i = 900K','T_i = 700K','T_i = 500K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblediameterxdir.png';
% saveas(gcf,filesave)

%% Bubble diameter z-direction 2 mm
figure(3)
box on
grid on
hold on
plot(t_sim(10:50),Dia_bubz((10:50),1),'LineWidth',2)
plot(t_sim(10:45),Dia_bubz((10:45),2),'LineWidth',2)
plot(t_sim(10:40),Dia_bubz((10:40),3),'LineWidth',2)
xlabel('Time [s]','FontSize',14)
ylabel('Bubble diameter z-direction (m)','FontSize',14)
v=legend('T_i = 900K','T_i = 700K','T_i = 500K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblediameterzdir.png';
% saveas(gcf,filesave)

%% Bubble temperature 2 mm
figure(4)
box on
grid on
hold on
plot(t_sim((10:50),1),T_bub((10:50),1),'LineWidth',2)
plot(t_sim((10:50),1),T_bub((10:50),2),'LineWidth',2)
plot(t_sim((10:50),1),T_bub((10:50),3),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Bubble temperatue (K)','FontSize',14)
v=legend('T_i = 900K','T_i = 700K','T_i = 500K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubbletemperature.png';
% saveas(gcf,filesave)

%% Bubble volume 2 mm 
figure(5)
box on
grid on
hold on
plot(t_sim((10:length(t_sim)),1),V_bub((10:length(t_sim)),1),'LineWidth',2)
plot(t_sim((10:length(t_sim)),1),V_bub((10:length(t_sim)),2),'LineWidth',2)
plot(t_sim((10:length(t_sim)),1),V_bub((10:length(t_sim)),3),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Bubble volume (m^3)','FontSize',14)
v=legend('T_i = 900K','T_i = 600K','T_i = 500K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblevolume.png';
% saveas(gcf,filesave)

%% Bubble dimensionless temperature 2 mm
figure(6)
box on
grid on
hold on
plot(t_sim(ZERO1:length(t_sim),1),T_dim(ZERO1:length(t_sim),1),'LineWidth',2)
plot(t_sim(ZERO2:length(t_sim),1),T_dim(ZERO2:length(t_sim),2),'LineWidth',2)
plot(t_sim(ZERO3:length(t_sim),1),T_dim(ZERO3:length(t_sim),3),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Dimensionless temperatue (-)','FontSize',14)
v=legend('T_i = 900K','T_i = 700K','T_i = 500K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubbletemperature_dim.png';
% saveas(gcf,filesave)

%% Heat transfer equivalent diameter at 900 K
figure(7)
box on
grid on
hold on
plot(t_sim(ZERO1:50,1),HEAT_900(1:30,3),'LineWidth',2)
plot(t_sim(ZERO2:50,1),HEAT_900(1:30,6),'LineWidth',2)
ylim([0 1200])
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
v=legend('h_{sim,900K}','h_{DH,900K}');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_equivalent900.png';
% saveas(gcf,filesave)

%% Heat transfer equivalent diameter at 700 K
figure(8)
box on
grid on
hold on
plot(t_sim(ZERO3:45,1),HEAT_700(1:25,3),'LineWidth',2)
plot(t_sim(ZERO1:45,1),HEAT_700(1:25,6),'LineWidth',2)
ylim([0 1200])
xlabel('Time [s]','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
v=legend('h_{sim,700K}','h_{DH,700K}');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_equivalent700.png';
% saveas(gcf,filesave)

%% Heat transfer equivalent diameter at 500 K
figure(9)
box on
grid on
hold on
plot(t_sim(ZERO2:40,1),HEAT_500(1:20,3),'LineWidth',2)
plot(t_sim(ZERO3:40,1),HEAT_500(1:20,6),'LineWidth',2)
ylim([0 1200])
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
v=legend('h_{sim,500K}','h_{DH,500K}');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_equivalent500.png';
% saveas(gcf,filesave)

%% Heat transfer equivalent diameter 1 mm, 900 K
figure(10)
box on
grid on
hold on
plot(t_sim(ZERO2:60,1),h_sim31mm(:,1),'LineWidth',2)
plot(t_sim(ZERO2:60,1),h_bub31mm(:,1),'LineWidth',2)
ylim([0 1400])
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
v=legend('h_{sim, 900K, 1mm}','h_{DH, 900K, 1mm}');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_equiv1mm.png';
% saveas(gcf,filesave)

%% Heat transfer equivalent diameter 1 mm, 900 K
box on
figure(11)
hold on
plot(t_sim(ZERO1:50,1),HEAT_900(1:30,3),'-square')
plot(t_sim(ZERO2:50,1),HEAT_900(1:30,6),'-o')
plot(t_sim(ZERO1:60,1),h_sim31mm(:,1),'-square')
plot(t_sim(ZERO1:60,1),h_bub31mm(:,1),'-o')
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
legend('h_{sim, 900K, 2mm}','h_{DH, 900K, 2mm}','h_{sim, 900K, 1mm}','h_{DH, 900K, 1mm}')
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_equivalent1mm.png';
% saveas(gcf,filesave)

%% Heat transfer (bed) equivalent diameter 1 mm, 900 K
box on
figure(12)
hold on
plot(t_sim(ZERO1:50,1),HEAT_900(1:30,3),'-square')
plot(t_sim(ZERO2:50,1),h_bed(1:30,1),'-o')
plot(t_sim(ZERO2:60,1),h_sim31mm(:,1),'-square')
plot(t_sim(ZERO2:60,1),h_bed1mm(:,1),'-o')
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
legend('h_{sim,900K, 2mm}','h_{bed,900K, 2mm}','h_{sim,900K, 1mm}','h_{bed,900K, 1mm}')
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\heattransfer_equiv_HBED1mm.png';
% saveas(gcf,filesave)

%% Bubble equivalent diameter 1 mm
figure(13)
box on
grid on
hold on
plot(t_sim(10:50),d_bub((10:50),1),'LineWidth',2)
plot(t_sim(10:length(t_sim)),d_bub1mm((20:length(t_sim1mm))),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Equivalent bubble diameter (m)','FontSize',14)
v=legend('d_p = 2 mm','d_p = 1 mm');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblediameterequivalent1mm.png';
% saveas(gcf,filesave)

%% Bubble diameter x-direction 1 mm
figure(14)
box on
grid on
hold on
plot(t_sim(10:50),Dia_bubx((10:50),1),'LineWidth',2)
plot(t_sim(10:length(t_sim)),Dia_bubx1mm((20:length(t_sim1mm))),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Bubble diameter x-direction (m)','FontSize',14)
v=legend('d_p = 2 mm','d_p = 1 mm');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblediameterxdir1mm.png';
% saveas(gcf,filesave)

%% Bubble diameter z-direction 1 mm
figure(15)
box on
grid on
hold on
plot(t_sim(10:50),Dia_bubz((10:50),1),'LineWidth',2)
plot(t_sim(10:length(t_sim),1),Dia_bubz1mm((20:length(t_sim1mm))),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Bubble diameter z-direction (m)','FontSize',14)
v=legend('d_p = 2 mm','d_p = 1 mm');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblediameterzdir1mm.png';
% saveas(gcf,filesave)

%% Bubble temperature 1 mm
figure(16)
box on
grid on
hold on
plot(t_sim((10:length(t_sim)),1),T_bub((10:length(t_sim)),1),'LineWidth',2)
plot(t_sim((10:length(t_sim)),1),T_bub1mm((20:length(t_sim1mm))),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Bubble temperatue (K)','FontSize',14)
v=legend('d_p = 2 mm','d_p = 1 mm');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubbletemperature1mm.png';
% saveas(gcf,filesave)

%% Bubble volume 1 mm
figure(17)
box on
grid on
hold on
plot(t_sim((10:length(t_sim)),1),V_bub((10:length(t_sim)),1),'LineWidth',2)
plot(t_sim((10:length(t_sim)),1),V_bub1mm((20:length(t_sim1mm))),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Bubble volume (m^3)','FontSize',14)
v=legend('d_p = 2 mm','d_p = 1 mm');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubblevolume1mm.png';
% saveas(gcf,filesave)


%% Dimensionless temperature 1 mm
figure(18)
box on
grid on
hold on
plot(t_sim(ZERO1:length(t_sim),1),T_dim(ZERO1:length(t_sim),1),'LineWidth',2)
plot(t_sim(ZERO1:length(t_sim),1),T_dim1mm(ZERO1mm:length(t_sim1mm)),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Dimensionless temperatue (-)','FontSize',14)
v=legend('d_p = 2 mm','d_p = 1 mm');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\bubbletemperature_dim1mm.png';
% saveas(gcf,filesave)

%% Plot surface relation 2 mm
figure(19)
box on
grid on
hold on
plot(t_sim(ZERO2:50,1),HEAT_900(1:30,6),'LineWidth',2)
plot(t_sim(ZERO1:50,1),HEAT_900(1:30,1),'LineWidth',2)
plot(t_sim(ZERO1:50,1),HEAT_900(1:30,2),'LineWidth',2)
plot(t_sim(ZERO1:50,1),HEAT_900(1:30,3),'LineWidth',2)
ylim([0 1100])
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
v=legend('h_{DH, 900 K}','h_{z, 900 K}','h_{x, 900 K}','h_{equiv, 900 K}');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\surfacerelation2mm.png';
% saveas(gcf,filesave)

%% Plot surface relation 1 mm
figure(20)
box on
grid on
hold on
plot(t_sim(ZERO1:60,1),h_bub31mm(:,1),'LineWidth',2)
plot(t_sim(ZERO1:60,1),h_sim11mm(:,1),'LineWidth',2)
plot(t_sim(ZERO1:60,1),h_sim21mm(:,1),'LineWidth',2)
plot(t_sim(ZERO1:60,1),h_sim31mm(:,1),'LineWidth',2)
ylim([0 2000])
xlabel('Time (s)','FontSize',14)
ylabel('Heat transfer coefficient (W/m^2 K)','FontSize',14)
v=legend('h_{DH, 900 K}','h_{z, 900 K}','h_{x, 900 K}','h_{equiv, 900 K}');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\surfacerelation1mm.png';
% saveas(gcf,filesave)

