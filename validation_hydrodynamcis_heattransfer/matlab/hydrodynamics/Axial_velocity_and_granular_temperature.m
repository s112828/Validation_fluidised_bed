%%% This script is used to calculate the axial velocity and the bubble
%%% granular temperature at a height z=0.2 m

clear all; clc; close all;

%% Load validation figure data (from papers)

path = 'E:\TUe\Afstuderen\Simulations\dataset_axvel_06.csv';
M    = csvread(path);
M1   = M(:,1)-M(1,1);
M2   = M(:,2);

path = 'E:\TUe\Afstuderen\Simulations\dataset_grantemp_06_paper2.csv';
P    = csvread(path);
P1   = P(:,1)-P(1,1);
P2   = P(:,2);


%% Load data files
cd 'E:\TUe\Afstuderen\Simulations\u06_t10sec_succes\'

path        = 'E:\TUe\Afstuderen\Simulations\u06_t10sec_succes\dump5160000.liggghts_run';
columns     = 20;
headerlines = 9;
data1       = loaddata(path,columns,headerlines);
data1       = transpose(data1);
data1       = sortrows(data1,1);
t           = 100000;
files       = 5160000+t:t:10140000;
Np          = 9240;
limx        = 0.044/85;
x           = 0+limx:limx:0.044-limx;
q           = 1;

for k = 5160000+t:t:10140000
    
    matFileName = sprintf('dump%d.liggghts_run',k);
    if exist(matFileName, 'file')
        
        data = loaddata(matFileName,columns,headerlines);
        
    else
        fprintf('File %s does not exist.\n', matFileName);
    end
    
    data  = transpose(data);
    data  = sortrows(data,1);
    data1 = [data1 data];
    
end

%% Seperate Data

for j = 0:columns:length(files)*columns
    ID(:,q) = data1(:,1+j);
    X(:,q)  = data1(:,3+j);
    Y(:,q)  = data1(:,4+j);
    Z(:,q)  = data1(:,5+j);
    Vx(:,q) = data1(:,9+j);
    Vy(:,q) = data1(:,10+j);
    Vz(:,q) = data1(:,11+j);
    
    q=q+1;
end


%% Calculation Axial velocity

a=1;

for v = 0:6:(6*length(files))
    for n = 1:Np
        if  Z(n,a) <= 0.0203 && Z(n,a) >= 0.0197
            
            A(n,1+v) = ID(n,a);
            A(n,2+v) = X(n,a);
            A(n,3+v) = Z(n,a);
            A(n,4+v) = Vz(n,a);
            A(n,5+v) = Vx(n,a);
            A(n,6+v) = Vy(n,a);
            
        else
            
            A(n,1+v) = ID(n,a);
            A(n,2+v) = X(n,a);
            A(n,3+v) = Z(n,a);
            A(n,4+v) = 0;
            A(n,5+v) = 0;
            A(n,6+v) = 0;
            
        end
        
    end
    Sortx(:,(1:6)+v) = sortrows(A(:,((1:6)+v)),2);
    
    a=a+1;
end


%% Analyse on x

m       = 2;
tracer  = zeros((length(files)+1),length(x));
tracer1 = zeros((length(files)+1),length(x));
tracer2 = zeros((length(files)+1),length(x));
tracer3 = zeros((length(files)+1),length(x));
Test    = zeros((length(files)+1),length(x));

for r = 1:length(x)
    for y = 1:Np
        
        if Sortx(y,2) >= (x(r)-limx) && Sortx(y,2) < (x(r)+limx)

            Test(y,r)   = Sortx(y,4);
            Vpx1(y,r)   = Sortx(y,5);
            Vpy1(y,r)   = Sortx(y,6);
            Vpz1(y,r)   = Sortx(y,4);
            
            if Test(y,r)~=0
                tracer(1,r) = tracer(1,r)+1;
            else
                tracer(1,r) = tracer(1,r);
            end
            
            if Vpx1(y,r)~=0
                tracer1(1,r) = tracer1(1,r)+1;
            else
                tracer1(1,r) = tracer1(1,r);
            end
            
            if Vpy1(y,r)~=0
                tracer2(1,r) = tracer2(1,r)+1;
            else
                tracer2(1,r) = tracer2(1,r);
            end
            
            if Vpz1(y,r)~=0
                tracer3(1,r) = tracer3(1,r)+1;
            else
                tracer3(1,r) = tracer3(1,r);
            end
            
        else
            Test(y,r) = 0;
            
            Vpx1(y,r) = 0;
            Vpy1(y,r) = 0;
            Vpz1(y,r) = 0;
            
        end
        
    end
end

C(m-1,:)   = sum(Test);

Vpx(m-1,:) = sum(Vpx1);
Vpy(m-1,:) = sum(Vpy1);
Vpz(m-1,:) = sum(Vpz1);

for b = 8:6:(6*(length(files)+1)-2)
    for i = 1:length(x)
        for n = 1:Np
            
            if Sortx(n,b) >= (x(i)-limx) && Sortx(n,b) < (x(i)+limx)
                
                B(n,i)      = Sortx(n,(b+2));
                Vpx2(n,i)   = Sortx(n,(b+3));
                Vpy2(n,i)   = Sortx(n,(b+4));
                Vpz2(n,i)   = Sortx(n,(b+2));
                
                if  B(n,i)~=0
                    tracer(m,i) = tracer(m,i)+1;
                else
                    tracer(m,i) = tracer(m,i);
                end
                
                if  Vpx2(n,i)~=0
                    tracer1(m,i) = tracer1(m,i)+1;
                else
                    tracer1(m,i) = tracer1(m,i);
                end
                
                if  Vpy2(n,i)~=0
                    tracer2(m,i) = tracer2(m,i)+1;
                else
                    tracer2(m,i) = tracer2(m,i);
                end
                
                if  Vpz2(n,i)~=0
                    tracer3(m,i) = tracer3(m,i)+1;
                else
                    tracer3(m,i) = tracer3(m,i);
                end
                
            else
                B(n,i)     = 0;
                
                Vpx2(n,i)  = 0;
                Vpy2(n,i)  = 0;
                Vpz2(n,i)  = 0;
            end
            
        end
        
    end
        
    V   = sum(B);
    C   = [C;V];
    
    R11  = sum(Vpx2);
    R22  = sum(Vpy2);
    R33  = sum(Vpz2);
    
    Vpx  = [Vpx;R11];
    Vpy  = [Vpy;R22];
    Vpz  = [Vpz;R33];
    
    m    = m+1;
end

D                 = C./tracer;
D(find(isnan(D))) = 0;
Vel_sum           = sum(D);
Vel_ax            = Vel_sum./(length(files)+1);

D1 = Vpx./tracer1;
D2 = Vpy./tracer2;
D3 = Vpz./tracer3;

D1(find(isnan(D1)))  = 0;
D2(find(isnan(D2)))  = 0;
D3(find(isnan(D3)))  = 0;

Vel_sum1 = sum(D1);
Vel_sum2 = sum(D2);
Vel_sum3 = sum(D3);

V_xi = Vel_sum1./(length(files)+1);
V_yi = Vel_sum2./(length(files)+1);
V_zi = Vel_sum3./(length(files)+1);


for jj = 1:(length(files)+1)
    
    D11(jj,:)  = D1(jj,:)-V_xi(1,:);
    D22(jj,:)  = D2(jj,:)-V_yi(1,:);
    D33(jj,:)  = D3(jj,:)-V_zi(1,:);
    
end


D11(find(isnan(D11))) = 0;
D22(find(isnan(D22))) = 0;
D33(find(isnan(D33))) = 0;

Vel_sum4  = sum(D11.*D11);
Vel_sum5  = sum(D22.*D22);
Vel_sum6  = sum(D33.*D33);

V_xi1     = (Vel_sum4)./(length(files)+1);
V_yi2     = (Vel_sum5)./(length(files)+1);
V_zi3     = (Vel_sum6)./(length(files)+1);


V_bubzz   = 1/3.*V_xi1+1/3.*V_yi2 +1/3.*V_zi3;

%% Plot figures

poly = polyfit(x,Vel_ax,5);
poly2 = polyval(poly,x);

figure
box on
grid on
hold on
plot(x,Vel_ax,'s')
plot(M1,M2,'LineWidth',2)
xlim([0 0.044])
ylim([-0.30 0.3])
xlabel('Position along x (m)','FontSize',14)
ylabel('Axial velocity V_z (m/s)','FontSize',14)
v=legend('V_z(x), simulation','V_z(x), M�ller He et al.');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\AxialVelocity06.png';
% saveas(gcf,filesave)

poly3 = polyfit(x,V_bubzz,5);
poly4 = polyval(poly3,x);

figure
box on
grid on
hold on
plot(x,V_bubzz,'s')
% plot(x,poly4)
plot(P1,P2,'LineWidth',2)
xlim([0 0.044])
% ylim([0 0.016])
xlabel('Position along x (m)','FontSize',14)
ylabel('Granular temperature \theta_{zz} (m^2/s^2)','FontSize',14)
v=legend('\theta_{b}(x), simulation','\theta_{b}(x), M�ller et al.');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\GranularTemp06.png';
% saveas(gcf,filesave)


