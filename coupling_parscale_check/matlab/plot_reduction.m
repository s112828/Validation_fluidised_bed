clear all; close all; clc;

%% Validation Heat transfer bubble

cd E:\TUe\Afstuderen\Simulations\Reduction

R      = 6e-4;
radius = [0:6e-4/9:6e-4]./R;

t_sim  = load('u4_Ti_700_Tp_550','t_sim');
t_sim  = struct2cell(t_sim);
t_sim  = cell2mat(t_sim);

tPar   = load('u4_Ti_700_Tp_550','tPar');
tPar   = struct2cell(tPar);
tPar   = cell2mat(tPar);

Tparticle = [load('u4_Ti_700_Tp_550','Tparticle') load('u4_Ti_900_Tp_550','Tparticle') load('u4_Ti_1100_Tp_550','Tparticle')...
    load('u4_Ti_900_Tp_450','Tparticle')];
Tparticle = struct2cell(Tparticle);
Tparticle = cell2mat(Tparticle);

Av_part_temp2 = [load('u4_Ti_700_Tp_550','Av_part_temp2') load('u4_Ti_900_Tp_550','Av_part_temp2') load('u4_Ti_1100_Tp_550','Av_part_temp2')...
    load('u4_Ti_900_Tp_450','Av_part_temp2')];
Av_part_temp2 = struct2cell(Av_part_temp2);
Av_part_temp2 = cell2mat(Av_part_temp2);

Av_outer_temp2 = [load('u4_Ti_700_Tp_550','Av_outer_temp2') load('u4_Ti_900_Tp_550','Av_outer_temp2') load('u4_Ti_1100_Tp_550','Av_outer_temp2')...
    load('u4_Ti_900_Tp_450','Av_outer_temp2')];
Av_outer_temp2 = struct2cell(Av_outer_temp2);
Av_outer_temp2 = cell2mat(Av_outer_temp2);

solid_conv = [load('u4_Ti_700_Tp_550','Av_conv') load('u4_Ti_900_Tp_550','Av_conv') load('u4_Ti_1100_Tp_550','Av_conv')...
    load('u4_Ti_900_Tp_450','Av_conv')];
solid_conv = struct2cell(solid_conv);
solid_conv = cell2mat(solid_conv);

gas_conv = [load('u4_Ti_700_Tp_550','Av_conv2') load('u4_Ti_900_Tp_550','Av_conv2') load('u4_Ti_1100_Tp_550','Av_conv2')...
    load('u4_Ti_900_Tp_450','Av_conv2')];
gas_conv = struct2cell(gas_conv);
gas_conv = cell2mat(gas_conv);

Gas_rad = [load('u4_Ti_900_Tp_450','Countour_solid')];
Gas_rad = struct2cell(Gas_rad);
Gas_rad = cell2mat(Gas_rad);

Solid_rad = [load('u4_Ti_900_Tp_450','Countour_gas')];
Solid_rad = struct2cell(Solid_rad);
Solid_rad = cell2mat(Solid_rad);

Temp_rad = [load('u4_Ti_700_Tp_550','Countour_temp')];
Temp_rad = struct2cell(Temp_rad);
Temp_rad = cell2mat(Temp_rad);


%% Plot data Bubble validation
close all

figure(1)
box on
grid on
hold on
plot(tPar,solid_conv(:,:,1),'O','LineWidth',2)
plot(tPar,solid_conv(:,:,2),'--','LineWidth',2)
plot(tPar,solid_conv(:,:,3))
xlabel('Time (s)','FontSize',14)
ylabel('Solid conversion (-)','FontSize',14)
v=legend('T_i = 700K','T_i = 900K','T_i = 1100K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\solid_time.png';
% saveas(gcf,filesave)

%%
figure(2)
box on
grid on
hold on
plot(tPar,gas_conv(:,:,1),'O','LineWidth',2)
plot(tPar,gas_conv(:,:,2),'--','LineWidth',2)
plot(tPar,gas_conv(:,:,3))
xlabel('Time (s)','FontSize',14)
ylabel('Gas conversion (-)','FontSize',14)
v=legend('T_i = 700K','T_i = 900K','T_i = 1100K');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\gas_time.png';
% saveas(gcf,filesave)

%% 
figure(3)
box on
grid on
hold on
plot(tPar,Av_outer_temp2(:,:,1),'O','LineWidth',2)
plot(t_sim,Tparticle(:,:,1),'LineWidth',2)
plot(tPar,Av_part_temp2(:,:,1),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Solid conversion (-)','FontSize',14)
v=legend('Parscale outer','liggghts average','Parscale avarage');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_coupling1.png';
% saveas(gcf,filesave)

%% 
figure(4)
box on
grid on
hold on
plot(tPar,Av_outer_temp2(:,:,2),'O','LineWidth',2)
plot(t_sim,Tparticle(:,:,2),'LineWidth',2)
plot(tPar,Av_part_temp2(:,:,2),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
xlim([0 0.7])
ylabel('Temperature (K)','FontSize',14)
v=legend('Parscale outer','LIGGGHTS average','Parscale avarage');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_coupling2.png';
filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_coupling2.fig';
saveas(gcf,filesave)

%% 
figure(5)
box on
grid on
hold on
plot(tPar,Av_outer_temp2(:,:,3),'O','LineWidth',2)
plot(t_sim,Tparticle(:,:,3),'LineWidth',2)
plot(tPar,Av_part_temp2(:,:,3),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Temperature (K)','FontSize',14)
v=legend('Parscale outer','liggghts average','Parscale avarage');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_coupling3.png';
% saveas(gcf,filesave)

%% 
figure(6)
box on
grid on
hold on
plot(t_sim,Tparticle(:,:,1),'LineWidth',2)
plot(t_sim,Tparticle(:,:,2),'LineWidth',2)
plot(t_sim,Tparticle(:,:,3),'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Temperature (K)','FontSize',14)
v=legend('T_i = 700K','T_i = 900K','T_i = 1100K');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_multiple.png';
% saveas(gcf,filesave)

%% 
figure(7)
box on
grid on
hold on
plot(t_sim,Tparticle(:,:,4),'LineWidth',2)
plot(tPar,Av_outer_temp2(:,:,4),'O','LineWidth',2)
plot(t_sim,Tparticle(:,:,2),'LineWidth',2)
plot(tPar,Av_outer_temp2(:,:,2),'O','LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Temperature (K)','FontSize',14)
v=legend('T_p = 450 K','Par = 450 K','T_p = 550 K','Par = 550 K');
v=set(v,'FontSize',11,'Location','east');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_diff_init.png';
% saveas(gcf,filesave)

%%
figure(8)
box on
grid on
hold on
plot(tPar,solid_conv(:,:,4),'LineWidth',2)
plot(tPar,solid_conv(:,:,2),'O','LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Solid conversion (-)','FontSize',14)
v=legend('T_p = 450 K','T_p = 550 K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\solid_conversion.png';
% saveas(gcf,filesave)

%%
figure(9)
box on
grid on
hold on
plot(tPar,gas_conv(:,:,4),'LineWidth',2)
plot(tPar,gas_conv(:,:,2),'O','LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Gas conversion (-)','FontSize',14)
v=legend('T_p = 450 K','T_p = 550 K');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\gas_conversion.png';
% saveas(gcf,filesave)

%%
figure(10)
box on
grid on
hold on
plot(radius,Gas_rad(1,:),'LineWidth',2)
plot(radius,Gas_rad(2,:),'LineWidth',2)
plot(radius,Gas_rad(3,:),'LineWidth',2)
plot(radius,Gas_rad(4,:),'LineWidth',2)
plot(radius,Gas_rad(5,:),'LineWidth',2)
plot(radius,Gas_rad(6,:),'LineWidth',2)
plot(radius,Gas_rad(7,:),'LineWidth',2)
plot(radius,Gas_rad(8,:),'LineWidth',2)
xlabel('r/r_p (-)','FontSize',14)
ylabel('Concentration gas (-)','FontSize',14)
v=legend('t=0 s','t=0.1 s','t=0.2 s','t=0.3 s','t=0.4 s','t=0.5 s','t=0.6 s','t=0.7 s');
v=set(v,'FontSize',11,'Location','northeast');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\gas_prof_particle.png';
% saveas(gcf,filesave)

%%
figure(11)
box on
grid on
hold on
plot(radius,Solid_rad(1,:),'LineWidth',2)
plot(radius,Solid_rad(2,:),'LineWidth',2)
plot(radius,Solid_rad(3,:),'LineWidth',2)
plot(radius,Solid_rad(4,:),'LineWidth',2)
plot(radius,Solid_rad(5,:),'LineWidth',2)
plot(radius,Solid_rad(6,:),'LineWidth',2)
plot(radius,Solid_rad(7,:),'LineWidth',2)
plot(radius,Solid_rad(8,:),'LineWidth',2)
xlabel('r/r_p (-)','FontSize',14)
ylabel('Concentration solid (-)','FontSize',14)
v=legend('t=0 s','t=0.1 s','t=0.2 s','t=0.3 s','t=0.4 s','t=0.5 s','t=0.6 s','t=0.7 s');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\solid_prof_particle.png';
% saveas(gcf,filesave)

%%
figure(12)
box on
grid on
hold on
plot(radius,Temp_rad(1,:),'LineWidth',2)
plot(radius,Temp_rad(2,:),'LineWidth',2)
plot(radius,Temp_rad(3,:),'LineWidth',2)
plot(radius,Temp_rad(4,:),'LineWidth',2)
plot(radius,Temp_rad(5,:),'LineWidth',2)
plot(radius,Temp_rad(6,:),'LineWidth',2)
plot(radius,Temp_rad(7,:),'LineWidth',2)
plot(radius,Temp_rad(8,:),'LineWidth',2)
xlabel('r/r_p (-)','FontSize',14)
ylabel('Temperature (K)','FontSize',14)
v=legend('t=0 s','t=0.1 s','t=0.2 s','t=0.3 s','t=0.4 s','t=0.5 s','t=0.6 s','t=0.7 s');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\temp_prof_particle.png';
% saveas(gcf,filesave)

