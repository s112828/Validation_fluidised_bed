%% Analysis reduction iron(III)oxide

clear all; close all; clc;

%% Dimensions initial bed

timestep = 75;

W     = 0.044;    % Width
dW    = W/18;     % Delta width
L     = 0.12;     % Length
dL    = L/36;     % Delta length
B     = 0.01;     % Depth
dB    = B/1;      % Delta depth
Lsi   = 0.03;
Ls    = 0.03;     % Static bed height
Vb    = W*B*Lsi;  % Volume bed
Vcell = dW*dB*dL; % Volume grid cell
A     = W*B;      % Massflow surface

x     = 0:dW:W;
y     = 0:dB:B;
z_tot = 0:dL:L;
z     = 0:dL:Ls;


%% Parameters gas (air)

U     = 4.5;        % Superficial gas velocity
rho_g = 0.0899;     % Density gas
nu    = 2e-04;      % Kinematic viscosity
g     = 9.81;       % Gravitational acceleration
p     = 101010.101; % Outlet pressure
Cp_g  = 14266;      % Heat capacity gas
k_g   = 0.3838;     % Thermal conductivity gas

%% Parameters solid 

Np      = 9200;              % Number of particles
dp      = 0.0012;            % Particle diameter
rp      = dp/2;              % Particle radius
phi     = 1;                 % Sphericity
rho_s   = 5200;              % Density solid
Vp      = (4/3)*pi*rp^3;     % Volume particle
Vs      = Np*Vp;             % Total volume solid
eps     = (Vb-Vs)/Vb;        % Volume fraction
eps_mf  = 0.4;
Cp_s    = 104;               % Heat capacity
k_s     = 0.58;              % Thermal conductivty
Tp_i    = 550;               % Initial particle temperature
Ap      = 4*pi*rp^2;         % Surface particle
Aps     = (6*(1-eps))/dp;    % Specific fluid-particle heat transfer surface
As      = Np*Aps;            % Specific fluid-particle heat transfer surface
gamma_b = 0.001;             % Volume of solids dispered in bubbles
eta_h   = 1;                 % adsorption efficiency, see book for argument
Conc_i  = 7;

%% LIGGGGHTS
%%% Load data particles

path         = 'E:\TUe\Afstuderen\Simulations\Reduction\Sim1_u4_Ti_900_Tp_550\post\dump130000.liggghts_run';
columns      = 22;
headerlines  = 9;
dataSim      = loaddata(path,columns,headerlines);
dataSim      = transpose(dataSim);
t            = 10000;
files        = 130000:t:870000;
starttime    = 0;
endtime      = 1;
time         = starttime:(endtime-starttime)/length(files):endtime;
q            = 1;


for k = 130000+t:t:870000;
    addpath('E:\TUe\Afstuderen\Simulations\Reduction\Sim1_u4_Ti_900_Tp_550\post')
    
    matFileName = sprintf('dump%d.liggghts_run',k);
    if exist(matFileName, 'file')
        cd E:\TUe\Afstuderen\Matlab\Scripts;
        dataSim1 = loaddata(matFileName,columns,headerlines);
        
    else
        fprintf('File %s does not exist.\n', matFileName);
    end
    
    dataSim1 = transpose(dataSim1);
    %     dataSim1 = sortrows(dataSim1,1);
    dataSim  = [dataSim dataSim1];
    
end


%% Calculate avarage particle temperature

for j = 0:columns:(length(files)-1)*columns
   
    Tp(:,q) = dataSim(:,19+j);
    
    q=q+1;
end

Tparticle = sum(Tp)./Np;


%% OpenFOAM
%%% Load gas variables

cd 'E:\TUe\Afstuderen\Simulations\Reduction\Sim1_u4_Ti_900_Tp_550\probes\0';

data2   = load('T_probes.txt');
Nprobes = 38;                       % Insert total number of probes
check1  = 0;
dt      = 1.0000e-05;

for J = 1:timestep
    
    check3 = 0;
    for I  = 1:Nprobes
        
        Tf(J,I) = data2(100+check1,2+check3);
        check3  = check3+1;
        
    end
    t_sim(J,:) = data2(100+check1,1);
    check1     = check1+100;
    
end

t_sim = transpose(t_sim);

%%  Parscale
%%% add the path to functions for postprocessing

cd 'E:\TUe\Afstuderen\Matlab\octave'
addpath('E:\TUe\Afstuderen\Matlab\octaveFunctions');

%% Load files

cd 'E:\TUe\Afstuderen\Matlab\Scripts'
run('parametersHetReaction.m')
relativePath  = 'E:\TUe\Afstuderen\Simulations\Reduction\Sim1_u4_Ti_900_Tp_550\pascal\';

fileToLoad1  = 'speciesSolid.json';
fileToLoad2  = 'speciesSolid.1.json';
fileToLoad3  = 'speciesSolid.2.json';
fileToLoad4  = 'speciesSolid.3.json';
fileToLoad5  = 'speciesA.json';
fileToLoad6  = 'speciesA.1.json';
fileToLoad7  = 'speciesA.2.json';
fileToLoad8  = 'speciesA.3.json';
fileToLoad9  = 'heat.json';
fileToLoad10 = 'heat.1.json';
fileToLoad11 = 'heat.2.json';
fileToLoad12 = 'heat.3.json';

verificationFileName = '../verificationStats/errorConversion.json';
particleIndex        = 3;
nPlotPoints          = 100;
plotSkip             = 1;
Ngrid                = 10;

%% Calculation Thielle Modulus

%%% Ahrennius
Rc = 8.3144621;       % Gasconstante
EA = 76e3;            % Activation energy
A0 = 2.2e3;           % Frequency factor

k_v       = (A0*exp(-EA./(Rc.*Tparticle))).*Cs0;  % Reaction rate constant 
ThieleMod = R * sqrt((a*k_v)/(DeIA));             % Thiele modulus
N_sh      = k_mA * R/D_eA;                        % Mass transfer/diffusion
D_ratio   = DeIA/D_eA;                            % Ratio diffusions

disp('Diffusion time scale');
diffusionTimeScale = R.^2./DeIA;

disp('Reaction time scale');
reactionTimeScale = 1./(k_vIntr(timestep)*CA0);

%% Plot Thiele modulus

figure(1)
box on
grid on
hold on
plot(Tparticle,ThieleMod,'LineWidth',2)
xlabel('Temperature (K)','FontSize',14)
ylabel('Thiele Modulus (-)','FontSize',14)
% v=legend('T_i = 900 K');
% v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test3.png';
% saveas(gcf,filesave)

%% Load Parscala data

myFiles = dir([relativePath,'*0.*']);

validData 	    = 0;
ConversionSolid = zeros(9200,1);
ConversionGas   = zeros(9200,1);
TempParticle    = zeros(9200,1);
Tpart_outer     = zeros(9200,1);
Countour_solid  = ones(1,10).*Conc_i;
Countour_gas    = zeros(1,10);
Countour_temp   = ones(1,10).*Tp_i;

for iDir=1:size(myFiles,1)

    if(myFiles(iDir).isdir)
        
        validData = validData +1;
        
        %%% Insert species Solid %%%
        [xDat1, yDat1, misc1] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad1], ...
            'data',particleIndex);
        
        [xDat2, yDat2, misc2] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad2], ...
            'data',particleIndex);
        
        [xDat3, yDat3, misc3] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad3], ...
            'data',particleIndex);
        
        [xDat4, yDat4, misc5] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad4], ...
            'data',particleIndex);
        
        %%% Insert species Gas %%%
        [xDat5, yDat5, misc5] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad5], ...
            'data',particleIndex);
        
        [xDat6, yDat6, misc6] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad6], ...
            'data',particleIndex);
        
        [xDat7, yDat7, misc7] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad7], ...
            'data',particleIndex);
        
        [xDat8, yDat8, misc8] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad8], ...
            'data',particleIndex);
        
        %%% Insert temperature particels %%%
        [xDat9, yDat9, misc9] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad9], ...
            'data',particleIndex);
        
        [xDat10, yDat10, misc10] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad10], ...
            'data',particleIndex);
        
        [xDat11, yDat11, misc11] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad11], ...
            'data',particleIndex);
        
        [xDat12, yDat12, misc12] = jsonGetParScaleData([relativePath, myFiles(iDir).name,'\',fileToLoad12], ...
            'data',particleIndex);
        
        %%% Calculations %%%
        yDat_11 = [yDat1; yDat2; yDat3; yDat4];
        yDat_12 = cell2mat(yDat_11);
        yDat_13 = yDat_12(:,1:10);
        yDat_1 = sum(yDat_13(:,1:10)./10,2);
        
        yDat_21 = [yDat5; yDat6; yDat7; yDat8];
        yDat_22 = cell2mat(yDat_21);
        yDat_23 = yDat_22(:,1:10);
        yDat_2  = sum(yDat_23(:,1:10)./10,2);
        
        yDat_31 = [yDat9; yDat10; yDat11; yDat12];
        yDat_32 = cell2mat(yDat_31);
        yDat_33 = yDat_32(:,1:10);
        yDat_3  = sum(yDat_33(:,1:10)./10,2);
        
        yDat_33_outer = yDat_33(:,10);
        
        parScaleConversionSolid = 1-(yDat_1./Cs0);
        parScaleConversionGas   = 1-(yDat_2./Cs0);
        
        parScaleTime(validData) = str2num(myFiles(iDir).name);
        
        
    end
    
    Countour_solid  = [Countour_solid;yDat_13(1,:)]; 
    Countour_gas    = [Countour_gas;yDat_23(1,:)];
    Countour_temp   = [Countour_temp;yDat_33(1,:)];
    ConversionSolid = [ConversionSolid,parScaleConversionSolid];
    ConversionGas   = [ConversionGas,parScaleConversionGas];
    TempParticle    = [TempParticle,yDat_3];
    Tpart_outer     = [Tpart_outer,yDat_33_outer];
end

%%

radius          = (0:R./9:R)./R;
tPar            = [0,(1:size(myFiles,1))./100];

Countour_solid  = Countour_solid./Countour_solid(1,1);
Countour_gas    = Countour_gas./Countour_gas(8,10);
ConversionSolid = ConversionSolid(:,2:8);
ConversionGas   = ConversionGas(:,2:8);
TempParticle    = TempParticle(:,2:8);
Tpart_outer     = Tpart_outer(:,2:8);

Av_conv_Solid   = sum(ConversionSolid)./Np;
Av_conv         = [1,Av_conv_Solid];

Av_conv_Gas     = sum(ConversionGas)./Np;
Av_conv2        = [0,Av_conv_Gas];

Av_part_temp    = sum(TempParticle)./Np;
Av_part_temp2   = [550,Av_part_temp];

Av_outer_temp    = sum(Tpart_outer)./Np;
Av_outer_temp2   = [550,Av_outer_temp];

parScaleEndTime         = max(parScaleTime);
parScaleEndTimePosition = parScaleTime==parScaleEndTime;
parScaleFinalConversion = Av_conv_Solid(parScaleEndTimePosition);
parScaleEndTimeDimLess  = parScaleEndTime./reactionTimeScale ;
parScaleDimLessTime     = parScaleTime./reactionTimeScale ;

%% Save the key results

results.Cs0             = Cs0;
results.conversion      = parScaleFinalConversion;
results.time            = parScaleEndTime;

disp('final conversion:');
results.conversion

disp('final solids consumption:');
results.consumption = results.conversion*Cs0

disp('after:');
results.time

cd 'E:\TUe\Afstuderen\Simulations\Reduction\Sim1_u4_Ti_900_Tp_550\pascal'
jSonDat = savejson('',results, 'result.json');

%% Plot the analytical solution

cd 'E:\TUe\Afstuderen\Matlab\octaveFunctions'

omegaPlot    = linspace(0, parScaleEndTimeDimLess, nPlotPoints);

for iP=1:nPlotPoints
    xi_m(iP)       = Wen_xi_m_time(omegaPlot(iP),D_ratio,ThieleMod(timestep),N_sh);
    conversion(iP) = Wen_conversion (ThieleMod(timestep), xi_m(iP), N_sh, D_ratio, omegaPlot(iP));
end

%% Plots
figure(2)
box on
grid on
hold on
plot(omegaPlot,conversion,'b-','MarkerFaceColor','b','MarkerSize',10,'LineWidth',2)
xlabel('t./t_f (-)','FontSize',14)
ylabel('Conversion rate (-)','FontSize',14)
v=legend('T_i = 900 K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test1.png';
% saveas(gcf,filesave)


%%
figure(3)
box on
grid on
hold on
plot(tPar,Av_conv,'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Solid conversion (-)','FontSize',14)
v=legend('T_i = 900 K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test2.png';
% saveas(gcf,filesave)

%%
figure(4)
box on
grid on
hold on
plot(tPar,Av_conv2,'LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Solid conversion (-)','FontSize',14)
v=legend('T_i = 900 K');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test3.png';
% saveas(gcf,filesave)

%%
figure(5)
grid on
hold on
plot(tPar,Av_outer_temp2,'-O','LineWidth',2)
plot(t_sim,Tparticle,'b','LineWidth',2)
plot(tPar,Av_part_temp2,'-x','LineWidth',2)
xlabel('Time (s)','FontSize',14)
ylabel('Temperature (K)','FontSize',14)
v=legend('Parscale outer','liggghts average','Parscale avarage');
v=set(v,'FontSize',11);
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test4.png';
% saveas(gcf,filesave)

%%
figure(6)
box on
grid on
hold on
plot(radius,Countour_solid(1,:),'LineWidth',2)
plot(radius,Countour_solid(3,:),'LineWidth',2)
plot(radius,Countour_solid(5,:),'LineWidth',2)
plot(radius,Countour_solid(7,:),'LineWidth',2)
plot(radius,Countour_solid(9,:),'LineWidth',2)
plot(radius,Countour_solid(11,:),'LineWidth',2)
xlabel('r/r_p (-)','FontSize',14)
ylabel('Concentration solid (-)','FontSize',14)
v=legend('t=0 s','t=0.06 s','t=0.16 s','t=0.26 s','t=0.36 s','t=0.46 s');
v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test5.png';
% saveas(gcf,filesave)

%%
figure(7)
box on
grid on
hold on
plot(radius,Countour_gas(1,:),'LineWidth',2)
plot(radius,Countour_gas(3,:),'LineWidth',2)
plot(radius,Countour_gas(5,:),'LineWidth',2)
plot(radius,Countour_gas(7,:),'LineWidth',2)
plot(radius,Countour_gas(9,:),'LineWidth',2)
plot(radius,Countour_gas(11,:),'LineWidth',2)
xlabel('r/r_p (-)','FontSize',14)
ylabel('Concentration gas (-)','FontSize',14)
v=legend('t=0 s','t=0.06 s','t=0.16 s','t=0.26 s','t=0.36 s','t=0.46 s');v=set(v,'FontSize',11,'Location','northwest');
% filesave='E:\TUe\Afstuderen\Master_Thesis\images\reduction_test6.png';
% saveas(gcf,filesave)

%% Save data for comparison

% cd E:\TUe\Afstuderen\Simulations\Reduction
% save('u4_Ti_900_Tp_550','omegaPlot','conversion','t_sim','tPar','nPlotPoints','parScaleEndTimeDimLess','D_ratio','Av_conv',...
%     'Av_conv2','Av_part_temp2','Av_outer_temp2','Tparticle','Countour_solid','Countour_gas','Countour_temp')














