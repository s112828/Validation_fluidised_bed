% *.m file
clear
clc
more off

numberOfParticles = 9200

%================================================================
text = '[350,350,350,350,350,350,350,350,350,350, 1100]';

fid=fopen('0/heat.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "heat",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);
%================================================================
text = '[0.0006]';

fid=fopen('0/radius.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "radius",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);

%================================================================
text = '[0.1,0.1,0.1,0.1,0.0,0.1,0.1,0.1,0.0,0.1, 0.1]';


fid=fopen('0/gasPhaseFraction.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "gasPhaseFraction",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);
%================================================================

text = '[0,0,0,0,0,0,0,0,0,0, 2.12e-3]';
%text = '[0, 2.12e-3]';

fid=fopen('0/speciesA.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "speciesA",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);
%================================================================

text = '[0,0,0,0,0,0,0,0,0,0, 99]';
%text = '[0, 99]';

fid=fopen('0/speciesA_zero.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "speciesA",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);
%================================================================

text = '[26.26, 26.26, 26.26, 26.26, 26.26, 26.26, 26.26, 26.26, 26.26, 26.26, 0]';
%text = '[7, 0]';

fid=fopen('0/speciesSolid.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "species",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);
%================================================================

text = '[99,99,99,99,99,99,99,99,99,99, 0';
%text = '[99, 0]';

fid=fopen('0/speciesSolid_zero.json','w');

% header
fprintf(fid, ['{\n']);
fprintf(fid, ['    "name": "species",\n']);
fprintf(fid, ['    "data":\n']);
fprintf(fid, ['    {\n']);

for iP=1:numberOfParticles
    index=iP;
    fprintf(fid, ['        "',num2str(index), '":  ', text ]);
    if(iP==numberOfParticles)
        fprintf(fid, ['\n']);
    else
        fprintf(fid, [', \n']);
    end        
end

% footer
fprintf(fid, ['    }\n']);
fprintf(fid, ['}']);
fclose(fid);
%================================================================
