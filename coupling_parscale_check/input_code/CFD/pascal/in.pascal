
# Mesh
particle_mesh nGridPoints 10

# Number of particles
particle_data number_particles 9200

# Coupling
coupling liggghts

# Heat properties solid
model propertiesThermo heatThermalConductivity_solid 
model propertiesThermo heatCapacity_solid 
model propertiesThermo heatDensity_solid 


# Heat properties gas
model propertiesThermo heatThermalConductivity_gas
model propertiesThermo heatCapacity_gas
model propertiesThermo heatDensity_gas

# Fluid species
model propertiesThermo speciesADiffusivity
model propertiesThermo speciesATransferCoeff
model propertiesThermo speciesAPhaseFraction
model propertiesThermo speciesATortuosity

# Solid species
model propertiesThermo speciesSolidDiffusivity
model propertiesThermo speciesSolidTransferCoeff

# Equations
modelEqn 1DSpherical  heat                 BC0 0  BC1 2  writeVolAvgProp  

modelEqn 1DSpherical  speciesA     gas     BC0 0  BC1 2  writeVolAvgProp  
modelEqn 1DSpherical  speciesSolid solid   BC0 0  BC1 2  writeVolAvgProp 

#modelEqn ShrinkingCore  speciesA     gas    BC1 2  writeVolAvgProp  
#modelEqn ShrinkingCore  speciesSolid solid  BC1 2  writeVolAvgProp 

# Chemistry

modelchemistry SingleReaction reaction 


# Control
control outputTimeStep 0.1
control timeStep 5e-6

