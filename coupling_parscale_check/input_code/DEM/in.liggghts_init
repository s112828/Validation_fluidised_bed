# Particle packing by insertion and successive growing of particles


variable YM     equal 10e6	  # Youngs modulus
variable ff     equal 0.1  	  # friction factor particle-particle contact
variable frf    equal 0.02 	  # rolling friction factor
variable f_wall equal 1.0 	  # friction factor particle-wall contact
variable cor    equal 0.97  	  # restitution coefficient particle-particle contact

variable rad         equal 0.0006 # particle radius
variable diameter    equal 0.0012 # particle diameter
variable density     equal 5200   # solids material density
variable nParticles  equal 9200   # number of particles in the system

variable ntime   equal 125000     # number of time steps
variable noutput equal 10000      # output after each noutput time steps 

variable Nfreq   equal ${ntime}
variable Nrepeat equal round(1.0*${ntime})

atom_style	granular
atom_modify	map array
communicate	single vel yes

boundary	f f f
newton		off

units		si
processors	2 2 1

# Read the restart file
#read_restart    ../DEM/post/restart/liggghts.restart

region          reg block 0. 0.044 0. 0.01 0. 0.12 units box
create_box      1 reg

neighbor        0.001 bin
neigh_modify    delay 0


# Material properties required for granular pair styles 

fix 		m1 all property/global youngsModulus peratomtype ${YM} 
fix 		m2 all property/global poissonsRatio peratomtype 0.33 
fix 		m3 all property/global coefficientRestitution peratomtypepair 1 ${cor}  
fix 		m4 all property/global coefficientFriction peratomtypepair 1 ${ff} 


# pair style
pair_style  gran model hertz tangential history #Hertzian without cohesion
pair_coeff  * *

# timestep
timestep    0.000001

# walls
fix     xwalls1 all wall/gran model hertz tangential history primitive type 1 xplane 0.0
fix     xwalls2 all wall/gran model hertz tangential history primitive type 1 xplane 0.044
fix     ywalls1 all wall/gran model hertz tangential history primitive type 1 yplane 0.0
fix     ywalls2 all wall/gran model hertz tangential history primitive type 1 yplane 0.01
fix     zwalls1 all wall/gran model hertz tangential history primitive type 1 zplane 0.0
fix     zwalls2 all wall/gran model hertz tangential history primitive type 1 zplane 0.12

# gravity
fix         gravi all gravity 9.81 vector 0.0 0.0 -1.0      

# heat transfer
fix         ftco all property/global thermalConductivity peratomtype 0.58
fix         ftca all property/global thermalCapacity peratomtype 104
fix         heattransfer all heat/gran initial_temperature 550

# particle distributions and insertion
region      bc block 0. 0.044 0. 0.01 0. 0.035 units box     # implementation box (has to be in the simulation box)
fix         pts1 all particletemplate/sphere 15485863 atom_type 1 density constant ${density} radius constant ${rad}
fix         pdd1 all particledistribution/discrete 15485867 1 pts1 1.0

fix         ins all insert/pack seed 32452843 distributiontemplate pdd1 vel constant 0. 0. 0. insert_every 10000 overlapcheck yes all_in yes particles_in_region ${nParticles} region bc

# apply nve integration to all particles that are inserted as single particles
fix         integr all nve/sphere

# output settings, include total thermal energy
compute         rke all erotate/sphere
thermo_style    custom step atoms ke c_rke f_heattransfer vol
thermo          1000
thermo_modify   lost ignore norm no
compute_modify  thermo_temp dynamic yes

#insert the first particles
run             1
dump            dmp all custom 10000 post/dump.liggghts_init id type x y z ix iy iz vx vy vz fx fy fz omegax omegay omegaz radius f_heattransfer[0] #f_heatFlux[0] f_speciesC[0] f_speciesCFlux[0]

run             ${ntime}

write_restart   post/restart/liggghts.restart

